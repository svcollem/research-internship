open import Cubical.Foundations.Prelude

module DCPO.Constructions.BinarySum (ℓi : Level) where

open import Cubical.Foundations.Function
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Path
open import Cubical.Foundations.Structure

open import Cubical.Data.Empty as ⊥
open import Cubical.Data.Sigma
open import Cubical.Data.Sum as ⊎

open import Cubical.Functions.Embedding

open import Cubical.HITs.PropositionalTruncation as ∥₁

open import Cubical.Relation.Binary
open import Cubical.Relation.Binary.Order.Poset

open import DCPO ℓi

private
  variable
    ℓa ℓb ℓd ℓd1 ℓd2 ℓe ℓt ℓt' ℓt1 ℓt2 : Level

module _ {A : Type ℓa} {B : Type ℓb}
         (_⊑₁_ : A → A → Type ℓt) (_⊑₂_ : B → B → Type ℓt') where

  ⊑⊎ : A ⊎ B → A ⊎ B → Type (ℓ-max ℓt ℓt')
  ⊑⊎ (inl a) (inl a') = Lift {j = ℓt'} (a ⊑₁ a')
  ⊑⊎ (inr b) (inr b') = Lift {j = ℓt} (b ⊑₂ b')
  ⊑⊎ (inl a) (inr b) = ⊥*
  ⊑⊎ (inr b) (inl a) = ⊥*

module _ {A : Type ℓa} {B : Type ℓb}
         {_⊑₁_ : A → A → Type ℓt} {_⊑₂_ : B → B → Type ℓt'}
         (isPosetA : IsPoset _⊑₁_) (isPosetB : IsPoset _⊑₂_) where
  open IsPoset
  open BinaryRelation

  private
    _⊑_ = ⊑⊎ _⊑₁_ _⊑₂_

  ⊑⊎prop : isPropValued _⊑_
  ⊑⊎prop (inl a) (inl a') = isOfHLevelLift 1 (isPosetA .is-prop-valued a a')
  ⊑⊎prop (inr b) (inr b') = isOfHLevelLift 1 (isPosetB .is-prop-valued b b')
  ⊑⊎prop (inl a) (inr b) = isProp⊥*
  ⊑⊎prop (inr b) (inl a) = isProp⊥*

  ⊑⊎refl : isRefl _⊑_
  ⊑⊎refl (inl a) = lift (isPosetA .is-refl a)
  ⊑⊎refl (inr b) = lift (isPosetB .is-refl b)

  ⊑⊎trans : isTrans _⊑_
  ⊑⊎trans (inl a₁) (inl a₂) (inl a₃) (lift a₁⊑a₂) (lift a₂⊑a₃) = lift (isPosetA .is-trans a₁ a₂ a₃ a₁⊑a₂ a₂⊑a₃)
  ⊑⊎trans (inr b₁) (inr b₂) (inr b₃) (lift b₁⊑b₂) (lift b₂⊑b₃) = lift (isPosetB .is-trans b₁ b₂ b₃ b₁⊑b₂ b₂⊑b₃)
  ⊑⊎trans (inl _) (inl _) (inr _) _  ()
  ⊑⊎trans (inl _) (inr _) (inl _) () ()
  ⊑⊎trans (inl _) (inr _) (inr _) () _
  ⊑⊎trans (inr _) (inl _) (inl _) () _
  ⊑⊎trans (inr _) (inl _) (inr _) () ()
  ⊑⊎trans (inr _) (inr _) (inl _) _  ()

  ⊑⊎antisym : isAntisym _⊑_
  ⊑⊎antisym (inl a) (inl a') (lift a⊑a') (lift a'⊑a) = cong inl (isPosetA .is-antisym a a' a⊑a' a'⊑a)
  ⊑⊎antisym (inr b) (inr b') (lift b⊑b') (lift b'⊑b) = cong inr (isPosetB .is-antisym b b' b⊑b' b'⊑b)
  ⊑⊎antisym (inl _) (inr _) () _
  ⊑⊎antisym (inr _) (inl _) () _
         
  isPoset⊑⊎ : IsPoset _⊑_
  isPoset⊑⊎ .is-set = isSet⊎ (isPosetA .is-set) (isPosetB .is-set)
  isPoset⊑⊎ .is-prop-valued = ⊑⊎prop
  isPoset⊑⊎ .is-refl = ⊑⊎refl
  isPoset⊑⊎ .is-trans = ⊑⊎trans
  isPoset⊑⊎ .is-antisym = ⊑⊎antisym

module _ {A : Type ℓa} {B : Type ℓb}
         {_⊑₁_ : A → A → Type ℓt} {_⊑₂_ : B → B → Type ℓt'} where
  private
    _⊑_ = ⊑⊎ _⊑₁_ _⊑₂_

  isInlFamily : {I : Type ℓi} (α : I → A ⊎ B) → Type (ℓ-max (ℓ-max ℓi ℓa) ℓb)
  isInlFamily {I = I} α = (i : I) → Σ[ a ∈ A ] α i ≡ inl a

  isInrFamily : {I : Type ℓi} (α : I → A ⊎ B) → Type (ℓ-max (ℓ-max ℓi ℓa) ℓb)
  isInrFamily {I = I} α = (i : I) → Σ[ b ∈ B ] α i ≡ inr b

  directedFamilyIsNotInlAndInr : {I : Type ℓi} (α : I → A ⊎ B) (δ : isDirected _⊑_ α)
                               → isInlFamily α
                               → isInrFamily α
                               → ⊥
  directedFamilyIsNotInlAndInr α δ inlFamily inrFamily =
    ∥₁.rec isProp⊥
      (λ i → lower (⊎Path.encode _ _ (sym (snd (inlFamily i)) ∙ snd (inrFamily i))))
      (inhabitedIfDirected _⊑_ δ)

  notInlAndInrInDirectedFamily : {I : Type ℓi} {i j : I} {α : I → A ⊎ B}
                               → {a : A} {b : B}
                               → isDirected _⊑_ α
                               → α i ≡ inl a
                               → α j ≡ inr b
                               → ⊥
  notInlAndInrInDirectedFamily {I = I} {i = i} {j = j} {α = α} δ p q =
    ∥₁.rec isProp⊥
      (λ (k , αᵢ⊑αₖ , αⱼ⊑αₖ) → lem k (α k) αᵢ⊑αₖ αⱼ⊑αₖ)
      (semidirectedIfDirected _⊑_ δ i j)
    where
      lem : (k : I) (x : A ⊎ B)
          → α i ⊑ x
          → α j ⊑ x
          → ⊥
      lem k (inl a') _ αⱼ⊑αₖ = lower (subst (_⊑ inl a') q αⱼ⊑αₖ)
      lem k (inr b') αᵢ⊑αₖ _ = lower (subst (_⊑ inr b') p αᵢ⊑αₖ)

  toLeftFamily : {I : Type ℓi} (α : I → A ⊎ B)
               → isInlFamily α
               → I → A
  toLeftFamily α = fst ∘_

  toRightFamily : {I : Type ℓi} (α : I → A ⊎ B)
                → isInrFamily α
                → I → B
  toRightFamily α = fst ∘_

  leftFamilyIsDirected : {I : Type ℓi} (α : I → A ⊎ B) (δ : isDirected _⊑_ α)
                       → (inlFamily : isInlFamily α)
                       → isDirected _⊑₁_ (toLeftFamily α inlFamily)
  leftFamilyIsDirected α δ inlFamily =
    inhabitedIfDirected _⊑_ δ ,
    λ i j →
      ∥₁.map
        (λ (k , αᵢ⊑αₖ , αⱼ⊑αₖ) →
          k ,
          lower (subst2 _⊑_ (snd (inlFamily i)) (snd (inlFamily k)) αᵢ⊑αₖ) ,
          lower (subst2 _⊑_ (snd (inlFamily j)) (snd (inlFamily k)) αⱼ⊑αₖ))
        (semidirectedIfDirected _⊑_ δ i j)

  rightFamilyIsDirected : {I : Type ℓi} (α : I → A ⊎ B) (δ : isDirected _⊑_ α)
                        → (inrFamily : isInrFamily α)
                        → isDirected _⊑₂_ (toRightFamily α inrFamily)
  rightFamilyIsDirected α δ inrFamily =
    inhabitedIfDirected _⊑_ δ ,
    λ i j →
      ∥₁.map
        (λ (k , αᵢ⊑αₖ , αⱼ⊑αₖ) →
          k ,
          lower (subst2 _⊑_ (snd (inrFamily i)) (snd (inrFamily k)) αᵢ⊑αₖ) ,
          lower (subst2 _⊑_ (snd (inrFamily j)) (snd (inrFamily k)) αⱼ⊑αₖ))
        (semidirectedIfDirected _⊑_ δ i j)

  inlIsSup : {I : Type ℓi} {a : A} {α : I → A ⊎ B} (δ : isDirected _⊑_ α)
           → (inlFamily : isInlFamily α)
           → isSup _⊑₁_ a (toLeftFamily α inlFamily) 
           → isSup _⊑_ (inl a) α
  inlIsSup {a = a} δ inlFamily aIsSup =
    (λ i →
      subst (_⊑ inl a)
        (sym (snd (inlFamily i)))
        (lift (supIsUpperbound _⊑₁_ aIsSup i))) ,
    (λ where
      (inl v) vIsUpper →
        lift (supIsLowerboundOfUpperbounds _⊑₁_ aIsSup v
          (λ i → lower (subst (_⊑ inl v) (snd (inlFamily i)) (vIsUpper i))))
      (inr v) vIsUpper →
        ∥₁.rec isProp⊥*
          (λ i → subst (_⊑ inr v) (snd (inlFamily i)) (vIsUpper i))
          (inhabitedIfDirected _⊑_ δ))

  inrIsSup : {I : Type ℓi} {b : B} {α : I → A ⊎ B} (δ : isDirected _⊑_ α)
           → (inrFamily : isInrFamily α)
           → isSup _⊑₂_ b (toRightFamily α inrFamily) 
           → isSup _⊑_ (inr b) α
  inrIsSup {b = b} δ inrFamily bIsSup =
    (λ i →
      subst (_⊑ inr b)
        (sym (snd (inrFamily i)))
        (lift (supIsUpperbound _⊑₂_ bIsSup i))) ,
    (λ where
      (inl v) vIsUpper →
        ∥₁.rec isProp⊥*
          (λ i → subst (_⊑ inl v) (snd (inrFamily i)) (vIsUpper i))
          (inhabitedIfDirected _⊑_ δ)
      (inr v) vIsUpper →
        lift (supIsLowerboundOfUpperbounds _⊑₂_ bIsSup v
          (λ i → lower (subst (_⊑ inr v) (snd (inrFamily i)) (vIsUpper i)))))

  module _ (isSetA : isSet A) (isSetB : isSet B) where
    isPropIsInlFamily : {I : Type ℓi} (α : I → A ⊎ B) → isProp (isInlFamily α)
    isPropIsInlFamily α =
      isPropΠ
        (λ _ (a , p) (a' , q) →
          Σ≡Prop (λ _ →  isSet⊎ isSetA isSetB _ _)
            (isEmbedding→Inj isEmbedding-inl a a' (sym p ∙ q)))

    isPropIsInrFamily : {I : Type ℓi} (α : I → A ⊎ B) → isProp (isInrFamily α)
    isPropIsInrFamily α =
      isPropΠ
        (λ _ (b , p) (b' , q) →
          Σ≡Prop (λ _ →  isSet⊎ isSetA isSetB _ _)
            (isEmbedding→Inj isEmbedding-inr b b' (sym p ∙ q)))

    directedIsInlOrInrFamily : {I : Type ℓi} (α : I → A ⊎ B) (δ : isDirected _⊑_ α)
                            → isInlFamily α ⊎ isInrFamily α
    directedIsInlOrInrFamily {I = I} α δ =
      ∥₁.rec
        (isProp⊎ (isPropIsInlFamily α) (isPropIsInrFamily α) (directedFamilyIsNotInlAndInr α δ))
        (λ i → lem1 i (α i) (inspect α i))
        (inhabitedIfDirected _⊑_ δ)
      where
        lem1 : (i : I) (x : A ⊎ B) → Reveal α · i is x → isInlFamily α ⊎ isInrFamily α
        lem1 i (inl a) [ p ]ᵢ = inl λ j → lem2 j (α j) (inspect α j)
          where
            lem2 : (j : I) (x : A ⊎ B) → Reveal α · j is x → Σ[ a' ∈ A ] α j ≡ inl a'
            lem2 j (inl a') [ q ]ᵢ = a' , q
            lem2 j (inr b)  [ q ]ᵢ = ⊥.rec (notInlAndInrInDirectedFamily δ p q)
        lem1 i (inr b) [ p ]ᵢ = inr λ j → lem2 j (α j) (inspect α j)
          where
            lem2 : (j : I) (x : A ⊎ B) → Reveal α · j is x → Σ[ b' ∈ B ] α j ≡ inr b'
            lem2 j (inl a)  [ q ]ᵢ = ⊥.rec (notInlAndInrInDirectedFamily δ q p)
            lem2 j (inr b') [ q ]ᵢ = b' , q

module _ (D : DCPO ℓd ℓt) (E : DCPO ℓe ℓt') where
  private
    _⊑_ = ⊑⊎ (order D) (order E)

  _⊎ᵈᶜᵖᵒ_ : DCPO (ℓ-max ℓd ℓe) (ℓ-max ℓt ℓt')
  _⊎ᵈᶜᵖᵒ_ = dcpo (⟨ D ⟩ ⊎ ⟨ E ⟩) _⊑_ (isPoset⊑⊎ (posetAxioms D) (posetAxioms E)) dc
    where
      dc : isDirectedComplete _⊑_
      dc {α = α} δ =
        ⊎.rec
          (λ inlFamily →
            inl (∐ D (leftFamilyIsDirected α δ inlFamily)) ,
            inlIsSup δ inlFamily (∐isSup D (leftFamilyIsDirected α δ inlFamily)))
          (λ inrFamily →
            inr (∐ E (rightFamilyIsDirected α δ inrFamily)) ,
            inrIsSup δ inrFamily (∐isSup E (rightFamilyIsDirected α δ inrFamily)))
          (directedIsInlOrInrFamily (sethood D) (sethood E) α δ)
