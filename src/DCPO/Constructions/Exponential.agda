open import Cubical.Foundations.Prelude

module DCPO.Constructions.Exponential (ℓi : Level) where

open import Cubical.Foundations.Function
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Structure

open import Cubical.HITs.PropositionalTruncation as ∥₁

open import Cubical.Relation.Binary.Order.Poset

open import DCPO ℓi
open import DCPO.Constructions.Product ℓi

private
  variable
    ℓa ℓb ℓt ℓt' : Level

module _ (A : DCPO ℓa ℓt) (B : DCPO ℓb ℓt') where
  ⊑→ : DCPOMorphism A B → DCPOMorphism A B → Type (ℓ-max ℓa ℓt')
  ⊑→ f g = ⊑Π ⟨ A ⟩ (order B) (function f) (function g)

  module _ where
    open IsPoset
    private
      module poset = IsPoset (isPoset⊑Π ⟨ A ⟩ (order B) (λ _ → posetAxioms B))

    isPoset⊑→ : IsPoset ⊑→
    isPoset⊑→ .is-set = isSetDCPOMorphism
    isPoset⊑→ .is-prop-valued f g = poset.is-prop-valued (function f) (function g)
    isPoset⊑→ .is-refl f = poset.is-refl (function f)
    isPoset⊑→ .is-trans f g h f⊑g g⊑h = poset.is-trans _ _ _ f⊑g g⊑h
    isPoset⊑→ .is-antisym f g f⊑g g⊑f = DCPOMorphism≡ (poset.is-antisym _ _ f⊑g g⊑f)

  scottSup : {I : Type ℓi} (α : I → DCPOMorphism A B) (δ : isDirected ⊑→ α)
           → DCPOMorphism A B
  scottSup {I = I} α δ = scottcontinuousmap f c
    where
      open DCPOReasoning

      f : ⟨ A ⟩ → ⟨ B ⟩
      f = ∐ (Πᵈᶜᵖᵒ ⟨ A ⟩ (λ _ → B)) δ

      c : isContinuous A B f
      c J β ε = u , l
        where
          u : isUpperbound (order B) (f (∐ A ε)) (f ∘ β)
          u j =
            f (β j)                                                   ⊑⟨ B ⟩[ reflexivity B _ ]
            ∐ B (pointwiseFamilyIsDirected ⟨ A ⟩ (order B) δ (β j))   ⊑⟨ B ⟩[ ∐isMonotone B _ _ (λ i → monotonicty (α i) (∐isUpperbound A ε j)) ]
            ∐ B (pointwiseFamilyIsDirected ⟨ A ⟩ (order B) δ (∐ A ε)) ⊑⟨ B ⟩[ reflexivity B _ ]
            f (∐ A ε)                                                 ∎⟨ B ⟩

          l : isLowerboundOfUpperbounds (order B) (f (∐ A ε)) (f ∘ β)
          l v vIsUpper =
            f (∐ A ε)                                                 ⊑⟨ B ⟩[ reflexivity B _ ]
            ∐ B (pointwiseFamilyIsDirected ⟨ A ⟩ (order B) δ (∐ A ε)) ⊑⟨ B ⟩[ ∐isLowerBoundOfUpperbounds B (pointwiseFamilyIsDirected ⟨ A ⟩ (order B) δ (∐ A ε)) v lem₁ ]
            v                                                         ∎⟨ B ⟩
            where
              lem₂ : (i : I) → isUpperbound (order B) v (function (α i) ∘ β)
              lem₂ i j = function (α i) (β j) ⊑⟨ B ⟩[ ∐isUpperbound B (pointwiseFamilyIsDirected ⟨ A ⟩ (order B) δ (β j)) i ]
                         f (β j)              ⊑⟨ B ⟩[ vIsUpper j ]
                         v                    ∎⟨ B ⟩

              lem₁ : isUpperbound (order B) v (pointwiseFamily (function ∘ α) (∐ A ε))
              lem₁ i = pointwiseFamily (function ∘ α) (∐ A ε) i ⊑⟨ B ⟩[ reflexivity B _ ]
                       function (α i) (∐ A ε)                   ⊑⟨ B ⟩[ ≡→⊑ B (supPreservation (α i) ε) ]
                       ∐ B (imageIsDirected (α i) ε)            ⊑⟨ B ⟩[ ∐isLowerBoundOfUpperbounds B (imageIsDirected (α i) ε) v (lem₂ i) ]
                       v ∎⟨ B ⟩

  _→ᵈᶜᵖᵒ_ : DCPO (ℓ-max (ℓ-max (ℓ-max (ℓ-max (ℓ-suc ℓi) ℓa) ℓt) ℓb) ℓt') (ℓ-max ℓa ℓt')
  _→ᵈᶜᵖᵒ_ = dcpo (DCPOMorphism A B) ⊑→ isPoset⊑→ (λ {I} {α} δ → dc α δ)
    where
      dc : {I : Type ℓi} (α : I → DCPOMorphism A B) (δ : isDirected ⊑→ α)
         → hasSup ⊑→ α
      dc α δ = scottSup α δ ,
               (λ i a → ∐isUpperbound B (pointwiseFamilyIsDirected' ⟨ A ⟩ (λ _ → B) δ a) i) ,
               (λ v vIsUpper a →
                ∐isLowerBoundOfUpperbounds B (pointwiseFamilyIsDirected' ⟨ A ⟩ (λ _ → B) δ a)
                  (function v a)
                  (λ i → vIsUpper i a))
