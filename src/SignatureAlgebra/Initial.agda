open import Cubical.Foundations.Prelude

module SignatureAlgebra.Initial (ℓi : Level) where

open import SignatureAlgebra.Initial.Base ℓi public
open import SignatureAlgebra.Initial.Elimination ℓi public
open import SignatureAlgebra.Initial.Free ℓi public
open import SignatureAlgebra.Initial.Initiality ℓi public
