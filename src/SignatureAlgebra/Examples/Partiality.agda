open import Cubical.Foundations.Prelude

module SignatureAlgebra.Examples.Partiality (ℓi : Level) where

open import Cubical.Foundations.Function
open import Cubical.Foundations.Structure

open import Cubical.Data.Empty as ⊥
open import Cubical.Data.Sigma
open import Cubical.Data.Sum as ⊎
open import Cubical.Data.Unit

open import DCPO ℓi
open import DCPO.Constructions.BinaryProduct ℓi
open import DCPO.Constructions.Discrete ℓi
open import DCPO.Constructions.Lift ℓi
open import DCPO.Constructions.Product ℓi

open import SignatureAlgebra.Initial ℓi
open import SignatureAlgebra.Monomial ℓi
open import SignatureAlgebra.Signature ℓi
open import SignatureAlgebra.SignatureAlgebra ℓi
open import SignatureAlgebra.SignatureAlgebraOver ℓi

open import SignatureAlgebra.Examples.PointedDCPO ℓi

private
  variable
    ℓx ℓt' ℓd ℓt : Level

module _ (D : DCPO ℓd ℓt) where
  partialityΣ : Signature ℓ-zero ℓ-zero ℓd ℓt ℓ-zero ℓ-zero
  partialityΣ = ⊥Σ +ˢⁱᵍ D

  PartialityAlgebra : (ℓ ℓ' : Level) → Type (ℓ-max (ℓ-max (ℓ-max (ℓ-max (ℓ-suc ℓi) ℓd) ℓt) (ℓ-suc ℓ)) (ℓ-suc ℓ'))
  PartialityAlgebra = SignatureAlgebra partialityΣ

  partialityalgebra : (X : DCPO ℓd ℓt)
                    → (incl : ⟨ D ⟩ → ⟨ X ⟩)
                    → isContinuous D X incl
                    → (bot : ⟨ X ⟩)
                    → isLeast (order X) bot
                    → PartialityAlgebra ℓd ℓt
  partialityalgebra X incl inclIsCont bot botIsLeast =
    signaturealgebra
      (presignaturealgebra X
        (λ { (inl tt) _ → bot
           ; (inr tt) → incl ∘ lower ∘ snd})
        (λ { (inl tt) → constIsContinuous (⟦ constructors partialityΣ (inl tt) ⟧ₘ X) X
           ; (inr tt) →
              isContinuous-∘ (⟦ constructors partialityΣ (inr tt) ⟧ₘ X) D X
                incl
                (lower ∘ snd)
                inclIsCont
                (isContinuous-∘
                  (⟦ constructors partialityΣ (inr tt) ⟧ₘ X) (liftDCPO ℓ-zero ℓ-zero D) D
                  lower snd
                  (lowerIsContinuous ℓ-zero ℓ-zero D)
                  (sndIsContinuous (Πᵈᶜᵖᵒ ⊥* (λ _ → X)) (liftDCPO ℓ-zero ℓ-zero D)))}))
      λ _ ρ → botIsLeast (ρ tt)

  _⊥ : PartialityAlgebra (ℓ-max (ℓ-max (ℓ-suc ℓi) ℓd) ℓt) (ℓ-max (ℓ-max (ℓ-suc ℓi) ℓd) ℓt)
  _⊥ = initialSignatureAlgebra partialityΣ

module _ {D : DCPO ℓd ℓt} (X : PartialityAlgebra D ℓx ℓt') where
  bot : ⟨ X ⟩
  bot = least (forgetIncl {Σ = ⊥Σ} X)

  incl' : ⟨ D ⟩ → ⟨ X ⟩
  incl' = incl {Σ = ⊥Σ} X

  botIsLeast : isLeast (order (underlyingDCPO X)) bot
  botIsLeast = leastIsLeast (forgetIncl {Σ = ⊥Σ} X)
 