open import Cubical.Foundations.Prelude

module SignatureAlgebra.Signature (ℓi : Level) where

open import Cubical.Foundations.Structure

open import DCPO ℓi

open import SignatureAlgebra.Monomial ℓi

private
  variable
    ℓd ℓt ℓc ℓmb ℓmc ℓmct ℓv : Level

record PreSignature (ℓc ℓmb ℓmc ℓmct : Level)
    : Type (ℓ-max (ℓ-max (ℓ-max (ℓ-max (ℓ-suc ℓi) (ℓ-suc ℓc)) (ℓ-suc ℓmb)) (ℓ-suc ℓmc)) (ℓ-suc ℓmct)) where
  constructor presignature
  field
    constructorNames : Type ℓc
    constructors     : constructorNames → Monomial ℓmb ℓmc ℓmct
open PreSignature public

data Side (Σ : PreSignature ℓc ℓmb ℓmc ℓmct) (V : Type ℓv)
    : Type (ℓ-max (ℓ-max (ℓ-max (ℓ-max (ℓ-max (ℓ-suc ℓi) ℓc) (ℓ-suc ℓmb)) (ℓ-suc ℓmc)) (ℓ-suc ℓmct)) ℓv) where
  varₛ    : (x : V) → Side Σ V
  constrₛ : (c : constructorNames Σ)
          → (Monomial.B (constructors Σ c) → Side Σ V)
          → ⟨ Monomial.C (constructors Σ c) ⟩
          → Side Σ V

⟦_⟧ₛ : {Σ : PreSignature ℓc ℓmb ℓmc ℓmct} {V : Type ℓv}
     → Side Σ V
     → (D : DCPO ℓd ℓt)
     → ((c : constructorNames Σ) → ⟨ ⟦ constructors Σ c ⟧ₘ D ⟩ → ⟨ D ⟩)
     → (V → ⟨ D ⟩) → ⟨ D ⟩
⟦ varₛ x ⟧ₛ D interpretation ρ = ρ x
⟦ constrₛ cName f c ⟧ₛ D interpretation ρ =
  interpretation cName ((λ b → ⟦ f b ⟧ₛ D interpretation ρ) , c)

record Inequality (Σ : PreSignature ℓc ℓmb ℓmc ℓmct) (ℓv : Level)
    : Type (ℓ-max (ℓ-max (ℓ-max (ℓ-max (ℓ-max (ℓ-suc ℓi) ℓc) (ℓ-suc ℓmb)) (ℓ-suc ℓmc)) (ℓ-suc ℓmct)) (ℓ-suc ℓv)) where
  constructor inequality
  field
    V : Type ℓv
    lhs   : Side Σ V
    rhs   : Side Σ V
open Inequality public

record Signature (ℓc ℓmb ℓmc ℓmct ℓe ℓv : Level)
    : Type (ℓ-max (ℓ-max (ℓ-max (ℓ-max (ℓ-max (ℓ-max (ℓ-suc ℓi) (ℓ-suc ℓc)) (ℓ-suc ℓmb)) (ℓ-suc ℓmc)) (ℓ-suc ℓmct)) (ℓ-suc ℓe)) (ℓ-suc ℓv)) where
  constructor signature
  field
    preSignature    : PreSignature ℓc ℓmb ℓmc ℓmct
    inequalityNames : Type ℓe
    inequalities    : inequalityNames → Inequality preSignature ℓv
  open PreSignature preSignature public
open Signature public
