\documentclass{beamer}

\title{Formalizing Algebraic Effects using Domain Theory}

\author{\underline{Simcha van Collem}, Niels van der Weide, Herman Geuvers}

\institute{Radboud University Nijmegen}
\date{21 June 2024}

\usetheme{metropolis}

\usepackage{amsmath,amsthm,amssymb,amsfonts}
\usepackage{stmaryrd}
\usepackage{mathpartir}
\usepackage{semantic}

\usepackage{minted}
\setminted{autogobble, fontsize=\scriptsize, baselinestretch=1}

% Support for Agda code.
\usepackage{agda}

% Decrease the indentation of code.
\setlength{\mathindent}{0em}

% Use special font families without TeX ligatures for Agda code. (This
% code is inspired by a comment by Enrico Gregorio/egreg:
% https://tex.stackexchange.com/a/103078.)
\usepackage{fontspec}
\newfontfamily{\AgdaSerifFont}{Latin Modern Roman}
\newfontfamily{\AgdaSansSerifFont}{Latin Modern Sans}
\newfontfamily{\AgdaTypewriterFont}{Latin Modern Mono}
\renewcommand{\AgdaFontStyle}[1]{{\AgdaSansSerifFont{}#1}}
\renewcommand{\AgdaKeywordFontStyle}[1]{{\AgdaSansSerifFont{}#1}}
\renewcommand{\AgdaStringFontStyle}[1]{{\AgdaTypewriterFont{}#1}}
\renewcommand{\AgdaCommentFontStyle}[1]{{\AgdaTypewriterFont{}#1}}
\renewcommand{\AgdaBoundFontStyle}[1]{\textit{\AgdaSansSerifFont{}#1}}

% Workarounds for the fact that the Latin Modern Sans font does not
% support certain characters.
\usepackage{newunicodechar}
\newunicodechar{λ}{\ensuremath{\mathnormal{\lambda}}}
\newunicodechar{∀}{\ensuremath{\mathnormal{\forall}}}
\newunicodechar{₁}{\ensuremath{{}_1}}
\newunicodechar{≡}{\ensuremath{\equiv}}
\newunicodechar{⟨}{\ensuremath{\langle}}
\newunicodechar{⟩}{\ensuremath{\rangle}}
\newunicodechar{∎}{\ensuremath{\qedsymbol}}

\AtBeginEnvironment{code}{\fontsize{9}{12}\selectfont}

\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usepackage{tikz-cd}
\usetikzlibrary{shadows}

%**
% \PutAt<overlay spec>[<box width>]{(<x>, <y>)}{<content>}
%
% real absolute positioning of <content> on a slide, if content is a figure,
% minipage or whatever kind of LR-box, the <box width> argument may be omitted
%
%
% implementation notes: 
%   - based on   \usepackage[absolute,overlay]{textpos}
%   - NOT combinable with any beamer feature that is based on pgfpages
%     (such as dual-screen support, built-in 2up handouts, etc.), as textpos 
%     and pgfpates interfere at the shippout-level.
%

\newcommand<>{\PutAt}[3][0pt]{%
{\only#4{\begin{textblock*}{#1}#2%
  #3
\end{textblock*}}}%
}

\newcommand<>{\NormalBox}[2][]{%
  \only#3{\tikz[#1, every node/.style={shape=rectangle,draw,fill=white, drop shadow, #1}]\node []{#2};}
}

\newcommand<>{\OrangeBox}[2][]{%
  \onslide#3{\NormalBox[fill=orange!30,draw=black!30,rounded corners=4pt,#1]{#2}}%
}

\begin{document}

\begin{code}[hide]
infix 4 _≡_
data _≡_ {ℓ : _} {A : Set ℓ} (x : A) : A → Set ℓ where
  refl : x ≡ x

ap : ∀ {ℓ ℓ'} {A : Set ℓ} {B : Set ℓ'} {x y : A} (f : A → B)
   → x ≡ y
   → f x ≡ f y
ap f refl = refl

_∙_ : ∀ {ℓ} {A : Set ℓ} {x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ∙ q = q

≡⟨⟩-syntax : ∀ {ℓ} {A : Set ℓ} (x : A) {y z} → y ≡ z → x ≡ y → x ≡ z
≡⟨⟩-syntax x q p = p ∙ q

infixr 2 ≡⟨⟩-syntax
syntax ≡⟨⟩-syntax x q p = x ≡⟨ p ⟩ q

_≡⟨⟩_ : ∀ {ℓ} {A : Set ℓ} (x : A) {y : A} → x ≡ y → x ≡ y
x ≡⟨⟩ x≡y = x≡y

_∎ : ∀ {ℓ} {A : Set ℓ} (x : A) → x ≡ x
x ∎ = refl

infixr 2 _≡⟨⟩_
infix  3 _∎
\end{code}

\frame{\titlepage}

\begin{frame}
  \frametitle{Programming language semantics}

  \begin{itemize}
    \item Understand the meaning of the syntax
    \item Prove properties for
          \begin{itemize}
            \item Compiler optimization
            \item Undefinability of certain functions
          \end{itemize}
    \item Denotational semantics
    \item \mintinline{haskell}{fac :: Int -> Int}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Domain theory}

  \begin{itemize}
    \item \textbf{Domain}: set $D$ with information ordering $\sqsubseteq$
    \item Smaller elements contain less information
  \end{itemize}

  % https://q.uiver.app/#q=WzAsOCxbMywyLCJcXGJvdCJdLFszLDAsIjAiXSxbNCwwLCIxIl0sWzUsMCwiMiJdLFsyLDAsIi0xIl0sWzEsMCwiLTIiXSxbMCwwLCJcXGRvdHMiXSxbNiwwLCJcXGRvdHMiXSxbMCw1LCIiLDAseyJzdHlsZSI6eyJoZWFkIjp7Im5hbWUiOiJub25lIn19fV0sWzAsNCwiIiwyLHsic3R5bGUiOnsiaGVhZCI6eyJuYW1lIjoibm9uZSJ9fX1dLFswLDEsIiIsMix7InN0eWxlIjp7ImhlYWQiOnsibmFtZSI6Im5vbmUifX19XSxbMCwyLCIiLDIseyJzdHlsZSI6eyJoZWFkIjp7Im5hbWUiOiJub25lIn19fV0sWzAsMywiIiwyLHsic3R5bGUiOnsiaGVhZCI6eyJuYW1lIjoibm9uZSJ9fX1dLFswLDcsIiIsMix7InN0eWxlIjp7ImhlYWQiOnsibmFtZSI6Im5vbmUifX19XSxbMCw2LCIiLDIseyJzdHlsZSI6eyJoZWFkIjp7Im5hbWUiOiJub25lIn19fV1d
  \[\begin{tikzcd}
    \dots & {-2} & {-1} & 0 & 1 & 2 & \dots \\
    \\
    &&& \bot
    \arrow[no head, from=3-4, to=1-1]
    \arrow[no head, from=3-4, to=1-2]
    \arrow[no head, from=3-4, to=1-3]
    \arrow[no head, from=3-4, to=1-4]
    \arrow[no head, from=3-4, to=1-5]
    \arrow[no head, from=3-4, to=1-6]
    \arrow[no head, from=3-4, to=1-7]
  \end{tikzcd}\]
\end{frame}

\begin{frame}
  \frametitle{Algebraic effects}

  \begin{itemize}
    \item Computations may have effects
          \begin{itemize}
            \item Partiality
            \item Non-determinism
            \item IO
          \end{itemize}
    \item Control-flow
    \item Composable
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Contributions}

  \begin{itemize}
    \item Framework for
      \begin{itemize}
        \item describing algebraic effects
        \item constructing them in domain theory
      \end{itemize}

    \item Constructively

    \item Examples
    
    \item Formalized in the Cubical Agda proof assistant
  \end{itemize}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Constructivism}

  \begin{itemize}
    \item Halting problem
    \item Classical mathematics: every statement either holds or doesn't hold
  \end{itemize}

  % https://q.uiver.app/#q=WzAsOCxbMywyLCJcXGJvdCJdLFszLDAsIjAiXSxbNCwwLCIxIl0sWzUsMCwiMiJdLFsyLDAsIi0xIl0sWzEsMCwiLTIiXSxbMCwwLCJcXGRvdHMiXSxbNiwwLCJcXGRvdHMiXSxbMCw1LCIiLDAseyJzdHlsZSI6eyJoZWFkIjp7Im5hbWUiOiJub25lIn19fV0sWzAsNCwiIiwyLHsic3R5bGUiOnsiaGVhZCI6eyJuYW1lIjoibm9uZSJ9fX1dLFswLDEsIiIsMix7InN0eWxlIjp7ImhlYWQiOnsibmFtZSI6Im5vbmUifX19XSxbMCwyLCIiLDIseyJzdHlsZSI6eyJoZWFkIjp7Im5hbWUiOiJub25lIn19fV0sWzAsMywiIiwyLHsic3R5bGUiOnsiaGVhZCI6eyJuYW1lIjoibm9uZSJ9fX1dLFswLDcsIiIsMix7InN0eWxlIjp7ImhlYWQiOnsibmFtZSI6Im5vbmUifX19XSxbMCw2LCIiLDIseyJzdHlsZSI6eyJoZWFkIjp7Im5hbWUiOiJub25lIn19fV1d
  \[\begin{tikzcd}
    \dots & {-2} & {-1} & 0 & 1 & 2 & \dots \\
    \\
    &&& \bot
    \arrow[no head, from=3-4, to=1-1]
    \arrow[no head, from=3-4, to=1-2]
    \arrow[no head, from=3-4, to=1-3]
    \arrow[no head, from=3-4, to=1-4]
    \arrow[no head, from=3-4, to=1-5]
    \arrow[no head, from=3-4, to=1-6]
    \arrow[no head, from=3-4, to=1-7]
  \end{tikzcd}\]
\end{frame}

\begin{frame}
  \frametitle{Why formalizing in a proof assistant?}

  \begin{minipage}{0.5\textwidth}
    \begin{itemize}
      \item Trustworthiness
      \item Interactive
      \item Automation
      \item Reproducibility
    \end{itemize}
  \end{minipage} \hfill
  \begin{minipage}{0.45\textwidth}
    \begin{figure}
        \includegraphics[height=0.9\textwidth]{../images/4colorthm.png}
    \end{figure}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Process of formalizing}

  \begin{itemize}
    \item Write down \textbf{definitions}
    \item State \textbf{properties} as \textbf{types}
    \item Prove properties using \textbf{programs} as \textbf{proofs} \smash{\raisebox{.5\dimexpr\baselineskip+\itemsep+\parskip}{$\left.\rule{0pt}{.5\dimexpr2\baselineskip+1\itemsep+1\parskip}\right\}\footnotesize\begin{array}{@{}l@{}}\text{Curry-Howard} \\ \text{correspondence}\end{array}$}}
    \item Let \textbf{type checker} verify the proofs
  \end{itemize}
\end{frame}

\begin{frame}[t,fragile]
  \frametitle{Agda vs Haskell}

  \begin{center}
    Lists in Agda \quad\quad\quad\quad\quad\quad Lists in Haskell
  \end{center}
  \begin{table}
    \centering
    \setlength\tabcolsep{4pt}

    \begin{minipage}{0.48\textwidth}
      \centering
      \begin{code}
data List (A : Set) : Set where
  [] : List A
  _::_ : A → List A → List A
      \end{code}
    \end{minipage}%
    \hfill
    \begin{minipage}{0.48\textwidth}
      \centering
      \begin{minted}{haskell}
        data List a = []
                    | a : List a
      \end{minted}
    \end{minipage}
  \end{table}

  \begin{onlyenv}<2->
    \noindent\rule{\textwidth}{0.3pt}
    \begin{center}
      List concatenation in Agda
      \quad\quad
      List concatenation in Haskell
    \end{center}
    \begin{table}
      \centering
      \setlength\tabcolsep{4pt}

      \begin{minipage}{0.48\textwidth}
        \centering
        \begin{code}
_++_ : {A : Set}
     → List A → List A → List A
[] ++ ys = ys
(x :: xs) ++ ys = x :: (xs ++ ys)
        \end{code}
      \end{minipage}%
      \hfill
      \begin{minipage}{0.48\textwidth}
        \centering
        \begin{minted}{haskell}
          (++) :: List a -> List a -> List a
          [] ++ ys = ys
          (x : xs) ++ ys = x : (xs ++ ys)
        \end{minted}
      \end{minipage}
    \end{table}
  \end{onlyenv}
  
  
\end{frame}

\begin{frame}[t]
  \frametitle{Proving a property using Agda}

  \begin{itemize}
    \item By definition, $[] \mathop{++} xs = xs$
    \item We can prove $xs \mathop{++} [] = xs$ in Agda itself
  \end{itemize}

  \begin{code}
app-nil-r : {A : Set} (xs : List A)
          → xs ++ [] ≡ xs
  \end{code}

  \begin{onlyenv}<2->
    \vspace{-1em}
    \begin{code}
app-nil-r [] = [] ++ []
                 ≡⟨ refl ⟩
               [] ∎
    \end{code}
  \end{onlyenv}

  \begin{onlyenv}<3->
    \vspace{-1em}
    \begin{code}
app-nil-r (x :: xs) = (x :: xs) ++ []
                        ≡⟨ refl ⟩
                      x :: (xs ++ [])
                        ≡⟨ ap (x ::_) IH ⟩
                      x :: xs ∎
  where
    IH : xs ++ [] ≡ xs
    IH = app-nil-r xs
    \end{code}
  \end{onlyenv}
  \only<4->{\small Similar to Haskell ``proofs'', but this is verified by the type checker}
\end{frame}

\begin{frame}
  \frametitle{Framework}

  Recall: \\
  Framework for
  \begin{itemize}
    \item describing algebraic effects
    \item constructing them in domain theory
  \end{itemize}

  Running examples:
  \begin{itemize}
    \item Partiality
    \item Non-determinism
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Partiality}

  \begin{itemize}
    \item Programs may not terminate
    \item So a program has either no outcome or a specified result
    \item Given domain $D$, we want a domain $D_\bot$, such that
          \begin{itemize}
            \item For each result $d$ in $D$,
                  we have the \textbf{same result} in $D_\bot$; written as $\eta(d)$
            \item We have a result for \textbf{non-termination}: $\bot$ in $D_\bot$
            \item Non-termination gives the \textbf{least information}
          \end{itemize}
  \end{itemize}
\end{frame}

\newcommand{\plotkinPower}[1]{\mathcal{P}(#1)}
\newcommand{\formalUnion}{\cup}
\newcommand{\powerIncl}[1]{\{{#1}\}}

\begin{frame}
  \frametitle{Non-determinism}

  \begin{itemize}
    \item Non-deterministic programs could have \textbf{multiple} results
    \item So a program has a \textbf{set} of possible results
    \item Given a domain $D$, we want a domain $\plotkinPower{D}$ of possible results, such that
          \begin{itemize}
            \item For each result $d$ in $D$, we have the result set with a \textbf{unique} result $\powerIncl{d}$ in $\plotkinPower{D}$
            \item Given result sets $s_1, s_2$ in $\plotkinPower{D}$,
                  we can \textbf{combine} their results as $s_1 \formalUnion s_2$ in $\plotkinPower{D}$
            \item The union operation is
                  \begin{itemize}
                    \item commutative and associative: order of combining does not matter
                    \item idempotent: combining a result set with itself, adds nothing
                  \end{itemize}
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Domain algebras}

  We have
  \begin{itemize}
    \item A domain
    \item Operations on this domain
    \item Respecting certain (in)equalities \smash{\raisebox{.5\dimexpr\baselineskip+\itemsep+\parskip}{$\left.\rule{0pt}{.5\dimexpr2\baselineskip+1\itemsep+1\parskip}\right\}\text{Signature}$}}
  \end{itemize}

  We call this an \textbf{algebra} for a signature

  Goals:
  \begin{itemize}
    \item Represent signatures
    \item For arbitrary signature, construct a data type in Cubical Agda which is such an algebra
  \end{itemize}
  
\end{frame}

\newcommand{\Bool}{\mathsf{Bool}}

\begin{frame}[t]
  \frametitle{Operations}

  \begin{table}
    \centering
    \setlength\tabcolsep{4pt}

    \begin{minipage}{0.48\textwidth}
      \centering
      Partiality constructors
      \begin{itemize}
        \item \only<1>{$\bot : D_\bot$}
              \only<2-3>{$\bot : 1 \to D_\bot$}
              \only<4->{$\bot : D_\bot^0 \times 1 \to D_\bot$}
        \item \only<1-4>{$\eta : D \to D_\bot$}
              \only<5->{$\eta : D_\bot^0 \times D \to D_\bot$}
      \end{itemize}
    \end{minipage}%
    \hfill
    \begin{minipage}{0.48\textwidth}
      \centering
      Non-determinism constructors
      \begin{itemize}
        \item \only<1-5>{$\powerIncl{-} : D \to \plotkinPower{D}$}
              \only<6->{$\powerIncl{-} : \plotkinPower{D}^0 \times D \to \plotkinPower{D}$}
        \item \only<1>{$\formalUnion : \plotkinPower{D} \to \plotkinPower{D} \to \plotkinPower{D}$}
              \only<2-6>{$\formalUnion : \plotkinPower{D}^2 \to \plotkinPower{D}$}
              \only<7->{$\formalUnion : \plotkinPower{D}^2 \times 1 \to \plotkinPower{D}$}
      \end{itemize}
    \end{minipage}
  \end{table}

  \begin{onlyenv}<3->
    Operations take recursive arguments and/or parameters of other types.

    General form: $X^n \times C \to X$
    \begin{itemize}
      \item $n$ is the arity
      \item $C$ is the type of parameters
    \end{itemize}
  \end{onlyenv}
\end{frame}

\newcommand{\op}[1]{\mathsf{op}_{#1}}

\begin{frame}
  \frametitle{Terms}

  Given operations, we can write down terms, e.g.
  \begin{itemize}
    \item $\powerIncl{x}$
    \item $x \formalUnion y$
    \item $\eta(x)$
    \item $\bot$
    \item $\dots$
  \end{itemize}

  Note: terms are just \textbf{syntax}
\end{frame}

\newcommand{\formalIneq}[2]{{#1} \mathop{\widetilde{\sqsubseteq}} {#2}}
\newcommand{\formalEq}[2]{{#1} \mathop{\widetilde{=}} {#2}}

\begin{frame}
  \frametitle{(In)equalities}

  A formal equality is a pair of terms $\formalEq{t_1}{t_2}$

  A formal inequality is a pair of terms $\formalIneq{t_1}{t_2}$

  Implicitly generalize over free variables in left- and right-hand side

  \begin{itemize}
    \item $\formalUnion$ is commutative: $\formalEq{s_1 \formalUnion s_2}{s_2 \formalUnion s_1}$
    \item $\bot$ is the smallest element: $\formalIneq{\bot}{x}$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Data type}

  In Cubical Agda, we can simultaneously define the following
  \begin{itemize}
    \item A \textbf{datatype} $X$ with constructors for each operation
    \item An \textbf{ordering} on $X$ which includes all the (in)equalities
  \end{itemize}

  This gives us a domain with the operations and (in)equalities of the signature,
  i.e. an algebra

  Moreover, we can show that this algebra is \textbf{initial}
\end{frame}

\begin{frame}
  \frametitle{Conclusions}

  \begin{itemize}
    \item Framework for describing algebraic effects: \textbf{signatures}
          \begin{itemize}
            \item Operations
            \item (In)equalities
          \end{itemize}
    \item Fits examples like \textbf{partiality} and \textbf{non-determinism}
    \item \textbf{Data type} for arbitrary signature in Cubical Agda
  \end{itemize}
  
  \scriptsize\url{https://gitlab.science.ru.nl/svcollem/research-internship}
\end{frame}

\begin{frame}
  \frametitle{The end}

  \begin{center}
    \Huge Questions?
  \end{center}
\end{frame}

\end{document} 