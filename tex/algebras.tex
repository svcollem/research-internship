\section{Algebras}\label{sec:algebras}

In this section we formally introduce algebras for signatures.
We do this in two steps.
First we look at DCPO algebras which have the operations of the signature. 
We then consider those algebras in which the formal inequalities of the signature are valid.

We first consider prealgebras for a signature.
These consist of a DCPO together with maps interpreting the operations of the signature.

\begin{definition}[Prealgebra for a Signature]
    Let $\Sigma$ be a signature. A \textbf{prealgebra} for $\Sigma$ consists of
    \begin{itemize}
        \item an underlying DCPO $X$;
        \item for each constructor name $a : \sigConstrNames{\Sigma}$,
        a Scott continuous map, which interprets the operation
        \begin{gather*}
            \interpretation{X}{a} : \interp{\sigMonomialFam{\Sigma}(a)}(X) \to X
        \end{gather*}
    \end{itemize}
    The type of prealgebras for $\Sigma$ is denoted by $\PreAlg{\Sigma}$.
    Note that by \cref{remark:presig-alg}, these prealgebras correspond to algebras for a functor.
\end{definition}

Given two prealgebras for a signature, we consider the morphisms between them.
Following \cref{remark:presig-alg}, such a morphism should be a morphism between the carriers of the prealgebras,
such that it commutes with the interpretations of the prealgebras.

\begin{definition}[Signature Morphism]
    Let $X, Y$ be prealgebras for a signature $\Sigma$.
    A \textbf{signature morphism} from $X$ to $Y$ is a Scott continuous map $f : X \to Y$ such that,
    for each $a : \sigConstrNames{\Sigma}$, we have
    \begin{gather*}
        \interpretation{Y}{a} \circ \interp{\sigMonomialFam{\Sigma}(a)}(f) = f \circ \interpretation{X}{a}
    \end{gather*}
    That is, we have that $f$ commutes with all the operations.
    Using \cref{remark:presig-alg}, we conclude that $\PreAlg{\Sigma}$ is a category.
\end{definition}

We now want to study those prealgebras, in which the formal inequalities of the signature are valid.
To do so, we first need to give a semantic interpretations to the sides of these formal inequalities.

\begin{definition}[Interpretation of Sides]\label{def:sides-interpretation}
    Let $\Sigma$ be a presignature, $V$ a type of variables, $s : \Side_{\Sigma, V}$ a side,
    $X$ a prealgebra for $\Sigma$, and $\varAssignment : V \to X$ a variable assignment.
    We define $\interp{s}_{X, \varAssignment} : X$, the \textbf{interpretation} of $s$ in $X$,
    by recursion on $s$.
    \begin{mathpar}
        \interp{\var{x}}_{X, \varAssignment} = \varAssignment(x)

        \interp{\constr{a}{f}{c}}_{X, \varAssignment} = \interpretation{X}{a}((\lambda b.\ \interp{f(b)}_{X, \varAssignment}), c)
    \end{mathpar}
    Variables get interpreted via $\varAssignment$.
    The side $\constr{a}{f}{c}$, representing the application of the operation named $a$ with argument $f$ and $c$,
    is interpreted by performing the operation named $a$ in the prealgebra $X$.
    We often omit the subscript $X$, if it is clear from context which prealgebra we are working in.
\end{definition}

Note that this interpretation is natural in the prealgebra $X$.
\begin{proposition}
    Let $\Sigma$ be a presignature, $V$ a type of variables, and $s : \Side_{\Sigma, V}$ a side.
    Furthermore, let $X, Y$ be prealgebras for $\Sigma$ and $f : X \to Y$ a prealgebra morphism.
    Then, for each variable assignment $\varAssignment : V \to X$, we have that
    \begin{gather*}
        \interp{s}_{Y, f \circ \varAssignment} = f(\interp{s}_{X, \varAssignment})
    \end{gather*}
\end{proposition}
\begin{proof}
    We prove this by structural induction on $s$.
    In the case that $s$ is $\var{x}$, we conclude with reflexivity on $f(\varAssignment(x))$.
    Otherwise, if $s$ is $\constr{a}{g}{c}$, we have that
    \begin{align*}
        \interp{\constr{a}{g}{c}}_{Y, f \circ \varAssignment}
          &= \interpretation{Y}{a}((\lambda b.\ \interp{g(b)}_{Y, f \circ \varAssignment}), c) \\
          &= \interpretation{Y}{a}((\lambda b.\ f(\interp{g(b)}_{X, \varAssignment})), c) \\
          &= \interpretation{Y}{a}(\interp{\sigMonomialFam{\Sigma}(a)}(f)((\lambda b.\ \interp{g(b)}_{X, \varAssignment}), c)) \\
          &= f(\interpretation{X}{a}((\lambda b.\ \interp{g(b)}_{X, \varAssignment}), c)) \\
          &= f(\interp{\constr{a}{g}{c}}_{X, \varAssignment})
    \end{align*}
    Note that we can use the induction hypothesis on $g(b) : \Side_{\Sigma, V}$,
    as all $g(b)$ are structurally smaller than $s$.
\end{proof}

With this interpretation at hand, we define what it means for a formal inequality to be valid in a prealgebra.
Intuitively, a formal inequality is valid, if the left-hand side is less than or equal to the right hand side,
for all possible values for the variables. This is exactly captured by the following definition.

\begin{definition}[Formal Inequality Validity]\label{def:ineq-validity}
    Let $\formalIneqVar{s_1}{V}{s_2}$ be a formal inequality over $\Sigma$ with variables $V : \Universe$.
    Furthermore, let $X$ be a prealgebra for $\Sigma$.
    We say that this formal inequality is \textbf{valid} in $X$ if,
    for each $\varAssignment : V \to X$,
    \begin{gather*}
        \interp{s_1}_\varAssignment \sqsubseteq \interp{s_2}_\varAssignment
    \end{gather*}
    Note that this is an inequality in the underlying DCPO of $X$.
    Whenever such a formal inequality is valid in $X$,
    we write it using the usual notation for validity: $X \vDash \formalIneqVar{s_1}{V}{s_2}$.
\end{definition}

An algebra for a signature can now be defined as a prealgebra for that signature,
where all the formal inequalities are valid.

\begin{definition}[Algebra for a Signature]
    Let $\Sigma$ be a signature. An \textbf{algebra} for $\Sigma$ is a prealgebra for $\Sigma$,
    such that for each $j : \sigIneqNames{\Sigma}$, we have that $X \vDash \sigIneqFam{\Sigma}(j)$.
    We define $\Alg{\Sigma}$ to be the full subcategory of $\PreAlg{\Sigma}$,
    of those prealgebras where all $\sigIneqFam{\Sigma}(j)$ are valid.
\end{definition}

With these definitions at hand, let us look at the algebras for some interesting signatures.

\subsection{Pointed DCPO}
A pointed DCPO (or DCPPO for short) is a DCPO $D$ together with a least element.
That is, we have an element $\bot \in D$ such that the following holds.
\begin{gather}\label{eq:dcppo-least-elem}
    \forall x \in D.\, \bot \sqsubseteq x
\end{gather}
We can formulate this as a signature.
Before we do that, note that any type $X$ is equivalent to $(\bot \to X) \times 1 \to X$,
so we get a point of $X$ by adding an operation with arity $\bot$ and constant DCPO $1$.
\begin{example}\label{ex:dcppo-sig}
    We define $\dcppoSig$, the signature for DCPPOs.
    We add one operation with constructor name $\botName$ and monomial $(\bot \to X) \times 1$.
    This allows us to write down the side $\botSide$, which represents the least element.
    Finally, we add the following formal inequality to our signature.
    \begin{gather*}
        \formalIneq{\botSide}{x}
    \end{gather*}
    It says that the element represented by $\botSide$ is indeed the least element.
    This formal inequality thus exactly describes inequality \eqref{eq:dcppo-least-elem}.
\end{example}

We conclude that an algebra for $\dcppoSig$ consists of a DCPO $D$,
together with an element, which is the least element in $D$.
A map between algebras $D,E$ is a continuous map $D \to E$ which preserves the bottom element.

\begin{proposition}\label{prop:dcppo-as-alg}
    Algebras for $\dcppoSig$ correspond to pointed DCPOs.
\end{proposition}

\subsection{Power Algebras}
In \cref{sec:signatures}, we defined the signature for the Plotkin powertheory as a running example.
In that example, we added an inclusion operation for didactical reasons.
Here, we do not consider the inclusion operation and delay that to \cref{sec:initial-algebra-free-examples}.
We quickly recap the definition of the Plotkin powertheory and consider its algebras.

Given a fixed DCPO $D$, the Plotkin powertheory has a binary operation, which is commutative, associative and idempotent.
We call this operation the \emph{formal union}.

\begin{example}\label{ex:power-sig}
    We define $\powerSig$, the signature for the Plotkin powertheory.
    We add one operation with constructor name $\formalUnionName$ and monomial $(\Bool \to X) \times 1$.
    Given sides $s_1, s_2$, we write $s_1 \formalUnion s_2$ for the side representing the formal union of $s_1, s_2$.
    Finally, we add the following formal inequalities, to guarantee that formal union is commutative, associative and idempotent.
    \begin{gather}
        \formalIneq{x \formalUnion y}{y \formalUnion x}\label{eq:ex-formal-union-comm} \\
        \formalIneq{(x \formalUnion y) \formalUnion z}{x \formalUnion (y \formalUnion z)}\label{eq:ex-formal-union-assoc} \\
        \formalIneq{x \formalUnion x}{x}\label{eq:ex-formal-union-idemp1} \\
        \formalIneq{x}{x \formalUnion x}\label{eq:ex-formal-union-idemp2}
    \end{gather}
    Commutativity is given by \eqref{eq:ex-formal-union-comm},
    associativity follows from \eqref{eq:ex-formal-union-comm} and \eqref{eq:ex-formal-union-assoc},
    and idempotency is given by \eqref{eq:ex-formal-union-idemp1}, \eqref{eq:ex-formal-union-idemp2}.
\end{example}

An algebra for $\powerSig$ consists of a DCPO $D$, and a continuous map $\formalUnion : D \to D \to D$
which is commutative, associative and idempotent.
A map between algebras $D,E$ is a continuous map $D \to E$ which commutes with the formal union operations.

\begin{definition}[Power Algebra]\label{def:power-alg}
    An algebra for $\powerSig$ is a \textbf{power algebra}.
\end{definition}
