\section{Initial Algebra}\label{sec:initial-algebra}

We now want to construct the initial algebra for a signature.
In type theory, initial objects are usually constructed via inductive definitions.
However, to construct the initial algebra for a signature, a regular inductive type does not suffice.
The first problem is that we need to define two sorts: a type $X$ and a relation $R : X \to X \to \Universe$.
The first sort acts as the carrier of the algebra, while the second sort plays the role of the ordering relation.
These sorts need to be defined mutually, as $R$ is a relation on $X$ and $R$ is also used in the constructors of $X$,
to, for example, express that some family is directed with respect to the ordering $R$.
Since the type of $R$ depends on $X$, we cannot write them down as mutual inductive types.
Moreover, the constructors of the ordering relation $R$ need to be able to refer to the constructors of $X$,
for example, to guarantee that $X$ is directed complete.
Such types, where $X$ and $R$ are defined simultaneously,
the type of $R$ depends on $X$,
and the constructors of $R$ can refer to the constructors of $X$,
are called inductive-inductive types (IITs)~\cite{nordvallforsberg2013thesis}.

However, an IIT also is not strong enough to construct the initial algebra.
We also want our carrier type to be antisymmetric with respect to its ordering relation.
We therefore need to add equalities to $X$ which guarantee this.
This is done by using a quotient inductive type (QIT).
These two constructions, IITs and QITs, can be combined into a quotient inductive-inductive type (QIIT)~\cite{DBLP:conf/fossacs/AltenkirchCDKF18}.

To motivate the notions of QITs, IITs and QIITs, we start this section by giving some examples in \cref{sec:initial-algebra-qiit-examples}.
We then construct the initial algebra for a signature as a QIIT in \cref{sec:initial-algebra-qiit}.
We show its initiality in \cref{sec:initial-algebra-initiality} after showing the recursion and induction principles of the QIIT.
Finally, we construct the free algebra for a signature in \cref{sec:initial-algebra-free}.

\subsection{Quotient Inductive-Inductive Types}\label{sec:initial-algebra-qiit-examples}

As our first example, we consider a QIT which defines finite sets of natural numbers.
Its constructors are given in \cref{fig:qit-fin-set}.
The $\emptyList$ and $\cons{x}{xs}$ constructors are the same as one would expect for regular lists.
Apart from the regular point constructors, we also have path constructors, which makes sure that $\finSet$ actually is the type of finite sets of natural numbers.
First of all, \nameref{rule:fin-set-cons-comm} makes sure that the order of the elements in a finite set does not matter.
Secondly, \nameref{rule:fin-set-cons-dup} says that adding an element is idempotent.
Combining these two path constructors give us the structure we would expect.
However, by introducing these path constructor, we now have multiple paths between finite sets.
For example, we can identity the list $\cons{x}{\cons{x}{\emptyList}}$ with itself by reflexivity,
or by the composition of \nameref{rule:fin-set-cons-dup} and its inverse.
These paths are not equal to each other.
This introduces extra structure which is not present when we consider classical finite multisets.
We therefore add the path constructor \nameref{rule:fin-set-set}.
As this path constructor asserts that $\finSet$ is a set,
we typically write this by adding a rule with the conclusion $\isSet(\finSet)$.

\begin{figure}
    \begin{mathpar}
        \inferrule
            { }
            {\emptyList : \finSet}

        \inferrule
            {x : \Nat \and xs : \finSet}
            {\cons{x}{xs} : \finSet}
        \\
        \inferrule
            [\textsc{Cons-Comm}]
            {x, y : \Nat \and xs : \finSet}
            {\cons{x}{\cons{y}{xs}} = \cons{y}{\cons{x}{xs}}}
            [\textsc{Cons-Comm}]\label{rule:fin-set-cons-comm}

        \inferrule
            [\textsc{Cons-Dup}]
            {x : \Nat \and xs : \finSet}
            {\cons{x}{\cons{x}{xs}} = \cons{x}{xs}}
            [\textsc{Cons-Dup}]\label{rule:fin-set-cons-dup}
        \\
        \inferrule
            [FinSet-Set]
            {xs, ys : \finSet \and p, q : xs = ys}
            {p = q}
            [\textsc{FinSet-Set}]\label{rule:fin-set-set}
    \end{mathpar}
    \caption{Constructors for $\finSet$}\label{fig:qit-fin-set}
\end{figure}

Next up, we shift our focus to an IIT.
As an example, we consider the type of sorted lists~\cite[Example 3.2]{nordvallforsberg2013thesis}.
Its constructors are given in \cref{fig:iit-sorted-list}.
The predicate $x \leqList xs$ asserts that $x$ is smaller than all elements of the sorted list $xs$.
This predicate allows us to define the type $\sortedList$.
We have that $\emptyList$ is sorted, and if $xs$ is sorted and we have a proof $p$ of the fact that $x \leqList xs$,
then we have that $\consSorted{x}{p}{xs}$ is again a sorted list.
To define the predicate, we know that $x$ is always smaller than all elements of the empty list.
Furthermore, if $p$ is a proof of the fact that $x \leqList xs$ and $n \leq x$,
then we can also conclude that $n$ is smaller than all elements of $\consSorted{x}{p}{xs}$.

\begin{figure}
    \begin{mathpar}
        \inferrule
            { }
            {\emptyList : \sortedList}
    
        \inferrule
            {x : \Nat \and xs : \sortedList \and p : x \leqList xs}
            {\consSorted{x}{p}{xs} : \sortedList}
        \\
        \inferrule
            {x : \Nat}
            {x \leqList \emptyList}
    
        \inferrule
            {n, x : \Nat \and xs : \sortedList \and p : x \leqList xs}
            {n \leq x \to n \leqList \consSorted{x}{p}{xs}}
    \end{mathpar}
    \caption{Constructors for $\sortedList$}\label{fig:iit-sorted-list}
\end{figure}

Finally, we combine the previous two example into a QIIT.
This gives us sorted lists where any two consecutive elements are unequal.%
\footnote{Note that we could also achieve this by using the strict order $<$ on natural numbers in the previous example.}
Its constructors are given in \cref{fig:qiit-sorted-list}.
We again have the $\emptyList$ and $\consSorted{x}{p}{xs}$ constructors to create sorted lists
and corresponding constructors to show that these lists are sorted.
We now also add a constructor which states that $\consSorted{x}{q}{\consSorted{x}{p}{xs}}$ and $\consSorted{x}{p}{xs}$ are equal.
Again, like in the first example, we also add a set truncation constructor to remove the extra structure we are not interested in.

\begin{figure}
    \begin{mathpar}
        \inferrule
            { }
            {\emptyList : \strictSortedList}
    
        \inferrule
            {x : \Nat \and xs : \strictSortedList \and p : x \leqList xs}
            {\consSorted{x}{p}{xs} : \strictSortedList}
        \\
        \inferrule
            {x : \Nat \and xs : \strictSortedList \and p : x \leqList xs \and q : x \leqList \consSorted{x}{p}{xs}}
            {\consSorted{x}{q}{\consSorted{x}{p}{xs}} = \consSorted{x}{p}{xs}}
    
        \inferrule
            { }
            {\isSet(\strictSortedList)}
        \\
        \inferrule
            {x : \Nat}
            {x \leqList \emptyList}
    
        \inferrule
            {n, x : \Nat \and xs : \strictSortedList \and p : x \leqList xs}
            {n \leq x \to n \leqList \consSorted{x}{p}{xs}}
    \end{mathpar}
    \caption{Constructors for $\strictSortedList$}\label{fig:qiit-sorted-list}
\end{figure}

\subsection{Construction of the Initial Algebra}\label{sec:initial-algebra-qiit}

To construct the initial algebra for a signature $\Sigma$,
we simultaneously define a type $\Initial{\Sigma}$ and an ordering relation $\Leq{\Sigma}$ on $\Initial{\Sigma}$ as a QIIT.
We discuss their constructors in three steps.
First, we show that we have constructors to define a DCPO structure on $\Initial{\Sigma}$.
We then show that we have the constructors to lift this DCPO structure to a prealgebra structure for $\Sigma$.
Finally, we lift this structure to an algebra structure for $\Sigma$.

\begin{definition}
    We simultaneously define the type $\Initial{\Sigma} : \Universe$,
    and the ordering relation ${\Leq{\Sigma} : \Initial{\Sigma} \to \Initial{\Sigma} \to \Universe}$ as a QIIT.
    Their constructors are given in \cref{fig:initial-dcpo-constructors,fig:initial-prealg-constructors,fig:initial-alg-constructors}.
    The constructors of $\Initial{\Sigma}$ are presented as terms with a type,
    while the constructors of $\Leq{\Sigma}$ are given as inference rules.
\end{definition}

\begin{figure}
    \begin{mathpar}
        \inferrule[\textsc{Leq-Refl}]
            { }
            {x \Leq{\Sigma} x}
            [\textsc{Leq-Refl}]\label{rule:leq-refl}

        \inferrule[\textsc{Leq-Trans}]
            {x \Leq{\Sigma} y \and y \Leq{\Sigma} z}
            {x \Leq{\Sigma} z}
            [\textsc{Leq-Trans}]\label{rule:leq-trans}

        \inferrule[\textsc{Leq-Prop}]
            { }
            {\isProp(x \Leq{\Sigma} y)}
            [\textsc{Leq-Prop}]\label{rule:leq-prop}
        \\
        \initialAntiSym{\Sigma} : \prod_{x, y : \Initial{\Sigma}} x \Leq{\Sigma} y \to y \Leq{\Sigma} x \to x = y

        \initialIsSet{\Sigma} : \isSet(\Initial{\Sigma})
        \\
        \initialLub{\Sigma} : \prod_{\alpha : I \to \Initial{\Sigma}} \isDirected{\alpha} \to \Initial{\Sigma}
        \\
        \inferrule[\textsc{$\initialLub{\Sigma}$-Is-Sup-1}]
            {\alpha : I \to \Initial{\Sigma} \and \delta : \isDirected{\alpha}}
            {\prod_{i : I} \alpha(i) \Leq{\Sigma} \initialLub{\Sigma}(\alpha, \delta)}
            [\textsc{$\initialLub{\Sigma}$-Is-Sup-1}]\label{rule:leq-lub-is-upperbound}

        \inferrule[\textsc{$\initialLub{\Sigma}$-Is-Sup-2}]
            {\alpha : I \to \Initial{\Sigma} \and \delta : \isDirected{\alpha}}
            {\prod_{v : \Initial{\Sigma}} \isUpperbound(v, \alpha) \to \initialLub{\Sigma}(\alpha, \delta) \Leq{\Sigma} v}
            [\textsc{$\initialLub{\Sigma}$-Is-Sup-2}]\label{rule:leq-lub-is-lowerbound-of-upperbounds}
    \end{mathpar}
    \caption{Constructors for the DCPO structure}
    \label{fig:initial-dcpo-constructors}
    \vspace{\floatsep}

    \begin{mathpar}
        \app{a} : \interp{\sigMonomialFam{\Sigma}(a)}(\Initial{\Sigma}) \to \Initial{\Sigma}

        \inferrule[\textsc{$\app{}$-Is-Cont-1}]
            {a : \sigConstrNames{\Sigma} \and \alpha : I \to \interp{\sigMonomialFam{\Sigma}(a)}(\Initial{\Sigma})
                \and \delta : \isDirected{\alpha}}
            {\prod_{i : I} \app{a}(\alpha(i)) \Leq{\Sigma} \app{a}(\dcpoSup{\interp{\sigMonomialFam{\Sigma}(a)}(\Initial{\Sigma})} \alpha)}
            [\textsc{$\app{}$-Is-Cont-1}]\label{rule:leq-app-cont-upperbound}

        \inferrule[\textsc{$\app{}$-Is-Cont-2}]
            {a : \sigConstrNames{\Sigma} \and \alpha : I \to \interp{\sigMonomialFam{\Sigma}(a)}(\Initial{\Sigma})
                \and \delta : \isDirected{\alpha}}
            {\prod_{v : \Initial{\Sigma}} \isUpperbound(v, {\app{a}} \circ \alpha) \to \app{a}(\dcpoSup{\interp{\sigMonomialFam{\Sigma}(a)}(\Initial{\Sigma})} \alpha) \Leq{\Sigma} v}
            [\textsc{$\app{}$-Is-Cont-2}]\label{rule:leq-app-cont-lowerbound-of-upperbounds}
    \end{mathpar}
    \caption{Constructors for the prealgebra structure for $\Sigma$}
    \label{fig:initial-prealg-constructors}
    \vspace{\floatsep}

    \begin{mathpar}
        \inferrule[\textsc{Ineq-Valid}]
            {j : \sigIneqNames{\Sigma} \and \sigIneqFam{\Sigma}(j) = \formalIneqVar{s_1}{V}{s_2} \and \rho : V \to \Initial{\Sigma}}
            {\interp{s_1}_\varAssignment \Leq{\Sigma} \interp{s_2}_\varAssignment}
            [\textsc{Ineq-Valid}]\label{rule:leq-ineq-valid}
    \end{mathpar}
    \caption{Constructors for the algebra structure for $\Sigma$}
    \label{fig:initial-alg-constructors}
\end{figure}

The constructors for the DCPO structure on $\Initial{\Sigma}$ are given in \cref{fig:initial-dcpo-constructors}.
The rules \nameref{rule:leq-refl}, \nameref{rule:leq-trans} and \nameref{rule:leq-prop}
make sure that $\Leq{\Sigma}$ is reflexive, transitive and proposition valued.
Furthermore, we guarantee that $\Leq{\Sigma}$ is antisymmetric
and $\Initial{\Sigma}$ is a set,%
\footnote{We could do without the set truncation,
    as the underlying type of a poset is always a set~\cite[Definition 4.1]{JongEscardo23}.
    See also \url{https://github.com/martinescardo/TypeTopology/blob/02add316f8a78bc79e5687e4249d9f0174dae1d2/source/UF/Hedberg.lagda\#L90}.}
by adding path constructors.
Finally, the $\initialLub{\Sigma}$ constructor is supposed to give the supremum for directed families.
This is guaranteed by \nameref{rule:leq-lub-is-upperbound} and \nameref{rule:leq-lub-is-lowerbound-of-upperbounds}.
Note that we often write $\initialLub{\Sigma} \alpha$, in case we do not care about the specific proof of directedness.

\begin{lemma}\label{lemma:initial-dcpo-structure}
    The type $\Initial{\Sigma}$ has a DCPO structure.
\end{lemma}

Next up, the constructors for the prealgebra structure are given in \cref{fig:initial-prealg-constructors}.
The $\app{a}$ constructor introduces, for each $a : \sigConstrNames{\Sigma}$,
an operation corresponding to the monomial $\sigMonomialFam{\Sigma}(a)$.
Note that, by \cref{lemma:initial-dcpo-structure}, we can actually pass $\Initial{\Sigma}$
as an argument to the DCPO endo-functor $\interp{\sigMonomialFam{\Sigma}(a)}$.
To guarantee that each operation $\app{a}$ is continuous, we have the rules
\nameref{rule:leq-app-cont-upperbound} and \nameref{rule:leq-app-cont-lowerbound-of-upperbounds}.
Combining these rules we get
\begin{gather*}
    \inferrule
        {a : \sigConstrNames{\Sigma} \and \alpha : I \to \interp{\sigMonomialFam{\Sigma}(a)}(\Initial{\Sigma})
            \and \delta : \isDirected{\alpha}}
        {\isLeastUpperbound(\app{a}(\dcpoSup{\interp{\sigMonomialFam{\Sigma}(a)}(\Initial{\Sigma})} \alpha), {\app{a}} \circ \alpha)}
\end{gather*}
This is exactly what it means for $\app{a}$ to be continuous.

\begin{lemma}\label{lemma:initial-prealg-structure}
    The type $\Initial{\Sigma}$ has a prealgebra structure for the signature $\Sigma$.
\end{lemma}

Finally, we have one single constructor for the algebra structure for the signature $\Sigma$ given in \cref{fig:initial-prealg-constructors}.
The rule \nameref{rule:leq-ineq-valid} guarantees that all the formal inequalities of the signature are valid.
Note that, by \cref{lemma:initial-prealg-structure}, we have an algebra structure for $\sigPre{\Sigma}$,
so we can indeed interpret the left- and right-hand side of the formal inequality in $\Initial{\Sigma}$.

\begin{theorem}\label{theorem:initial-prealg-structure}
    The type $\Initial{\Sigma}$ has an algebra structure for the signature $\Sigma$.
\end{theorem}

\begin{remark}\label{remark:comparison-w-types-const-dcpo}
    If we unfold the definition of the interpretation of a monomial, we see that $\app{}$ has the following type
    \begin{gather*}
        \app{} : \prod_{a : \sigConstrNames{\Sigma}} (\sigMonomialArity{\Sigma}(a) \to \Initial{\Sigma}) \times \sigMonomialConst{\Sigma}(a) \to \Initial{\Sigma}
    \end{gather*}
    One may notice that this seems similar to the introduction rule for W-types~\cite{Lof84}.
    This raises the question why we need the family of DCPOs $\sigMonomialConst{\Sigma}$,
    and thus also the constant DCPO $C$ in the definition of monomials.
    Indeed, if $\sigMonomialConst{\Sigma}$ were to be a family of types,
    we could replace the type of constructor names with $\sum_{a : \sigConstrNames{\Sigma}} \sigMonomialConst{\Sigma}(a)$.
    This would result in having a separate $\sigMonomialConst{\Sigma}$ being redundant.
    However, $\sigMonomialConst{\Sigma}$ is a family of DCPOs,
    and the structure of these DCPOs would be lost if we would move the elements of $\sigMonomialConst{\Sigma}$ to the type of constructor names.
    Since we want to create Scott continuous maps $\interp{\sigMonomialFam{\Sigma}(a)}(\Initial{\Sigma}) \to \Initial{\Sigma}$,
    we certainly do not want to lose some structure of $\interp{\sigMonomialFam{\Sigma}(a)}(\Initial{\Sigma})$.
    Hence we actually need the constant DCPO in the definition of a monomial.
\end{remark}


\subsection{Initiality}\label{sec:initial-algebra-initiality}

To show that $\Initial{\Sigma}$ is initial in $\Alg{\Sigma}$,
we need to construct a unique morphism $\Initial{\Sigma} \to X$ for arbitrary $X$.
The existence of such a morphism comes from the recursion principle for $\Initial{\Sigma}$.

\begin{theorem}
    Let $\Sigma$ be a signature and $X$ an algebra for $\Sigma$.
    Then there exists a morphism ${\rec_{\Sigma, X} : \Initial{\Sigma} \to X}$,
    with the following computation rules
    \begin{align*}
        \rec_{\Sigma, X}(\app{a}(x)) &\doteq \interpretation{X}{a}(\interp{\sigMonomialFam{\Sigma}(a)}(\rec_{\Sigma, X})(x)) \\
        \rec_{\Sigma, X}(\initialLub{\Sigma} \alpha) &\doteq \dcpoSup{X} (\rec_{\Sigma,X} \circ \alpha)
    \end{align*}
\end{theorem}
\begin{proof}
    The two remaining constructors of $\Initial{\Sigma}$, $\initialAntiSym{\Sigma}$ and $\initialIsSet{\Sigma}$,
    get send to the proofs that $X$ has an antisymmetric relation and is a set, respectively.
    The function $\rec_{\Sigma,X}$ is well-defined, as it is monotone.
    It is an algebra morphism, as by definition, it is continuous and commutes with all the operations.
\end{proof}

To prove the uniqueness of such a morphism, we need an induction principle for $\Initial{\Sigma}$.
For this, we need \emph{displayed algebras}~\cite{DBLP:conf/popl/Sojakova15, DBLP:conf/rta/KaposiK18}.
In this report, we do not treat displayed algebras in its general form.
Rather, we only consider our specific use case.
First, let us define how a monomial acts on a family of types.
\begin{definition}
    Let $M$ be a monomial with arity $B$ and constant DCPO $C$,
    $X$ a DCPO, and $Y : X \to \Universe$ a family of types.
    The family $Y$ lifts to a type family $\action{M}(Y) : \interp{M}(X) \to \Universe$, which is defined as
    \begin{gather*}
        \action{M}(Y)(f, c) = \left(\prod_{b : B} Y(f(b))\right) \times C
    \end{gather*}
\end{definition}
The action defined above, allows us to state the induction hypotheses for $\Initial{\Sigma}$.
If we restrict our view to a predicate $Y : X \to \Universe$ and let $x : \interp{M}(X)$,
then $\action{M}(Y)(x)$ expresses the fact that $Y$ holds for all the subterms of type $X$ in $x$.
This is exactly what we need as the induction hypothesis in the case of $\app{a}(x) : \Initial{\Sigma}$.
\begin{theorem}\label{thm:initial-alg-ind}
    Let $Y : \Initial{\Sigma} \to \Universe$ be a family of types, such that
    \begin{itemize}
        \item each $Y(x)$ is a proposition;
        \item $Y$ is \emph{supremum preserving}: if $\alpha : I \to \Initial{\Sigma}$
              is a directed family, such that $Y(\alpha(i))$ holds for every $i : I$, then $Y(\initialLub{\Sigma}(\alpha))$;
        \item $Y$ is \emph{app preserving}: if $a$ is a constructor name,
              $x : \interp{\sigMonomialFam{\Sigma}(a)}(\Initial{\Sigma})$ and $\action{\sigMonomialFam{\Sigma}(a)}(Y)(x)$,
              then $Y(\app{a}(x))$ holds.
    \end{itemize}
    In that case $Y(x)$ holds for every $x : \Initial{\Sigma}$, that is,
    we have a function $\ind_{\Sigma,Y} : \prod_{x : \Initial{\Sigma}} Y(x)$.
\end{theorem}
\begin{proof}
    The function $\ind_{\Sigma,Y}$ is defined by pattern matching.
    For the $\initialLub{\Sigma}$ and $\app{a}$ constructors, we use that fact that $Y$ is supremum and app preserving.
    On the path constructors of $\Initial{\Sigma}$, $\ind_{\Sigma,Y}$ is defined using the fact that $Y$ is a proposition valued.
\end{proof}

With these elimination principles at hand, we show the initiality of $\Initial{\Sigma}$.
\begin{theorem}
    Let $\Sigma$ be a signature.
    The algebra $\Initial{\Sigma}$ is initial in the category $\Alg{\Sigma}$.
\end{theorem}
\begin{proof}
    Let $X$ be an arbitrary algebra for $\Sigma$.
    The unique algebra morphism $\bang : \Initial{\Sigma} \to X$ is given by the recursion principle.

    Now let $f : \Initial{\Sigma} \to X$ be an arbitrary map of algebras.
    To show that $\bang$ and $f$ are equal, it is enough to show that their underlying functions are equal.
    After applying function extensionality, it thus suffices to show that
    \begin{gather}\label{eqn:initiality-ind-statement}
        \prod_{x : \Initial{\Sigma}} \bang(x) = f(x)
    \end{gather}
    As the underlying type $X$ is a set, we know that \eqref{eqn:initiality-ind-statement} is a predicate.
    We can thus use the induction principle of $\Initial{\Sigma}$.
    First we prove that \eqref{eqn:initiality-ind-statement} is true for the supremum of a directed family.
    Let $\alpha : I \to \Initial{\Sigma}$ be a directed family.
    By the computation rules of $\bang$ and the continuity of $f$,
    we need to show that the suprema in $X$ of $\bang \circ \alpha$ and $f \circ \alpha$ are equal.
    This holds, because the families $\bang \circ \alpha$ and $f \circ \alpha$ are equal by the induction hypothesis.

    Finally, we need to show that \eqref{eqn:initiality-ind-statement} is true for any $\app{a}(x)$.
    By the computation rules of $\bang$ and the fact that $f$ commutes with all operations,
    we have to show that
    \begin{gather*}
        \interpretation{X}{a}(\interp{\sigMonomialFam{\Sigma}(a)}(\bang)(x)) = \interpretation{X}{a}(\interp{\sigMonomialFam{\Sigma}(a)}(f)(x))
    \end{gather*}
    This follows from the induction hypothesis, as \eqref{eqn:initiality-ind-statement} holds for all the subterms of $x$.
\end{proof}

\subsection{Free Algebra for a Signature}\label{sec:initial-algebra-free}

We now construct the free algebra for a signature $\Sigma$.
Let $U : \Alg{\Sigma} \to \DCPO$ be the forgetful functor.
We want to construct a left adjoint $F : \DCPO \to \Alg{\Sigma}$.
Given a DCPO $D$, we need an inclusion $D \to UFD$ which will act as the unit of the adjunction $F \dashv U$.
This tells us that we need to add an inclusion operation to the signature.

\begin{definition}
    Let $\Sigma$ be a signature and $D$ a DCPO.
    We define a signature $\sigOver{\Sigma}{D}$, by extending the constructor names of $\Sigma$ with a new element $\inclName$,
    and assign $(\bot \to X) \times D$ as the monomial for $\inclName$.
    We do not add any new inequalities, but note that we have to lift the original inequalities of $\Sigma$ as we changed the type of constructor names.
    We leave these technical details to the formalization.
\end{definition}

If $X : \Alg{\Sigma}$ is an algebra and $f : D \to U X$,
we construct an algebra for the signature $\sigOver{\Sigma}{D}$,
by using $f$ as the interpretation for the inclusion constructor name $\inclName$.
We write $\addIncl{X}{f} : \Alg{\sigOver{\Sigma}{D}}$ for this algebra.

Conversely, if $X : \Alg{\sigOver{\Sigma}{D}}$ is an algebra for $\sigOver{\Sigma}{D}$, we define an algebra for $\Sigma$,
by simply forgetting the interpretation of $\inclName$. We write $\forgetIncl{X} : \Alg{\Sigma}$ for this algebra.

These operations let us construct the free algebra for a signature $\Sigma$.
We do this by sending a DCPO $D$ to $\forgetIncl{(\Initial{\sigOver{\Sigma}{D}})}$.

\begin{theorem}
    The operation $D \mapsto \forgetIncl{(\Initial{\sigOver{\Sigma}{D}})}$ lifts to a functor $F : \DCPO \to \Alg{\Sigma}$,
    which is left adjoint to the forgetful functor $U : \Alg{\Sigma} \to \DCPO$.
\end{theorem}
\begin{proof}
    We constructor this adjunction using universal arrows.
    The unit of the adjunction, $\eta_D : D \to UFD$, is given by the inclusion operation $\app{\inclName}$.
    The universal arrows are constructed in the following way.
    Let $X : \Alg{\Sigma}$ be an algebra and $f : D \to UX$.
    By the initiality of $\Initial{\sigOver{\Sigma}{D}}$, we have a unique map $\Initial{\sigOver{\Sigma}{D}} \to \addIncl{X}{f}$.
    By forgetting about the fact that this map commutes with the inclusion operation,
    we get a map $FD \to X$, whose uniqueness follows from the initiality of $\Initial{\sigOver{\Sigma}{D}}$.
\end{proof}

\subsection{Coalesced Sum}
The categorical coproduct of two pointed DCPOs is defined as their coalesced sum.
Classically, one defines this by taking the union of the two DCPPOs,
removing both their bottom elements, and adding a new bottom element.
Here we take a different approach.
We first consider algebras with inclusion operations for both DCPPOs.
We then add inequalities such that the inclusions of both bottom elements become equal.
Finally, we define the coproduct as the initial algebra for this signature.

\begin{example}
    Let $D, E$ be DCPPOs. We define $\csumSig{D}{E}$.
    We add two operations, with constructor names $\sumInclName{D}, \sumInclName{E}$
    and corresponding monomials $(\bot \to X) \times D$ and $(\bot \to X) \times E$.
    This allows us two write down sides of the form $\sumIncl{D}(d), \sumIncl{E}(e)$ for $d : D, e : E$.
    Finally, we add the following two inequalities.
    \begin{gather*}
        \formalIneq{\sumIncl{D}(\bot_D)}{\sumIncl{E}(\bot_E)} \\
        \formalIneq{\sumIncl{E}(\bot_E)}{\sumIncl{D}(\bot_D)}
    \end{gather*}
\end{example}

An algebra for $\csumSig{D}{E}$ consists of a DCPO $X$,
together with inclusion operations $\sumIncl{D} : D \to X$ and $\sumIncl{E} : E \to X$,
such that $\sumIncl{D}(\bot_D) = \sumIncl{E}(\bot_E)$.

\begin{proposition}
    Let $D, E$ be DCPPOs. Their coproduct is given by the initial algebra for $\csumSig{D}{E}$.
\end{proposition}
\begin{proof}
    By taking $\sumIncl{D}(\bot_D) = \sumIncl{E}(\bot_E)$ as the bottom element and using \cref{thm:initial-alg-ind}.
\end{proof}

\subsection{Smash Product}
Let $D,E$ be pointed DCPOs. Their smash product is the product $D \times E$,
where $(\bot_D, e) = (d, \bot_E)$ for every $d : D, e : E$.
We define the smash product as the initial algebra for a signature with these specifications.

\begin{example}
    Let $D,E$ be DCPPOs. We define $\smashSig{D}{E}$.
    It has a single operation which includes $D \times E$, i.e. it is represented by the monomial $(\bot \to X) \times (D \times E)$.
    This allows us to write sides of the form $(d, e)$ for $d : D, e : E$.
    For each $d : D, e : E$, we add the following two inequalities.
    \begin{gather*}
        \formalIneq{(d, \bot_E)}{(\bot_D, e)} \\
        \formalIneq{(\bot_D, e)}{(d, \bot_E)}
    \end{gather*}
\end{example}

An algebra for $\smashSig{D}{E}$ consists of a DCPO $X$,
together with an inclusion $\smashIncl : D \times E \to X$,
such that $\smashIncl(\bot_D, e) = \smashIncl(d, \bot_E)$ for every $d : D, e : E$.

\begin{definition}[Smash Product]
    Let $D, E$ be DCPPOs. Their \textbf{smash product} is the initial algebra for $\smashSig{D}{E}$.
\end{definition}

\subsection{Coequalizer}
Let $D, E$ be DCPOs and $f, g : D \to E$ continuous maps.
We define the coequalizer by first defining a signature whose algebras correspond to coequalizer cocones,
and then considering the initial algebra for this signature.

\begin{example}
    Let $D,E$ be DCPOs and and $f, g : D \to E$. We define $\coeqSig{f}{g}$.
    It has a single operation which includes $E$, i.e. it is represented by the monomial $(\bot \to X) \times E$.
    This allows us to write sides of the form $\coeqIncl(e)$ for $e : E$.
    For each $d : D$, we add the following two inequalities.
    \begin{gather*}
        \formalIneq{\coeqIncl(f(d))}{\coeqIncl(g(d))} \\
        \formalIneq{\coeqIncl(g(d))}{\coeqIncl(f(d))}
    \end{gather*}
\end{example}

An algebra for $\coeqSig{f}{g}$ is a DCPO $X$,
together with a map $\coeqIncl : E \to X$,
such that $\coeqIncl(f(d)) = \coeqIncl(g(d))$ for each $d : D$.

\begin{proposition}
    Let $D,E$ be DCPOs and and $f, g : D \to E$.
    Algebras for $\coeqSig{f}{g}$ correspond to coequalizer cocones of $f$ and $g$.
    Furthermore, the initial algebra for $\coeqSig{f}{g}$ is the coequalizer of $f$ and $g$.
\end{proposition}

\subsection{Free Algebra Examples}\label{sec:initial-algebra-free-examples}

Recall the definitions of $\dcppoSig, \powerSig$, defined in \cref{ex:dcppo-sig,ex:power-sig} respectively.
we described their algebras in \cref{prop:dcppo-as-alg,def:power-alg}.
We now consider free algebras for these signatures.

\paragraph{Partiality}

The signature $\dcppoSig$ describes pointed DCPOs.
By the construction of free algebras and \cref{prop:dcppo-as-alg},
we have a functor $\flatDomain{(\blank)} : \DCPO \to \DCPPO$, which is left adjoint to the forgetful functor $U : \DCPPO \to \DCPO$.
This takes a DCPO, and freely adds a least element to it.

\paragraph{Power Domain}

Recall the definition of $\powerSig$, the signature for power algebras from \cref{ex:power-sig}.
We now consider free algebras for this signature.
Given a DCPO $D$, the free algebra for $\powerSig$ over $D$ is called the Plotkin powerdomain of $D$.
We denote it by $\plotkinPower{D}$. It has the following two operations.
\begin{gather*}
    \powerIncl{\blank} : D \to \plotkinPower{D} \\
    \formalUnion : \plotkinPower{D} \to \plotkinPower{D} \to \plotkinPower{D}
\end{gather*}
The formal union operation, $\formalUnion$, is commutative, associative, and idempotent.

\begin{figure}
    \begin{mathpar}
        \powerIncl{\blank} : D \to \plotkinPower{D}

        \formalUnion : \plotkinPower{D} \to \plotkinPower{D} \to \plotkinPower{D}

        \inferrule
            {\alpha_1, \alpha_2 : I \to \plotkinPower{D} \and \isDirected{\alpha_1} \and \isDirected{\alpha_2}}
            {\initialLub{} \alpha_1 \formalUnion \initialLub{} \alpha_2 = \initialLub{} (\lambda i.\, \alpha_1(i) \formalUnion \alpha_2(i))}

        \inferrule
            {\alpha : I \to D \and \isDirected{\alpha}}
            {\powerIncl{\dcpoSup{D} \alpha} = \initialLub{} (\powerIncl{\blank} \circ \alpha)}
    \end{mathpar}
    \caption{Operations of $\plotkinPower{D}$ and their continuity}
    \label{fig:power-doamin-qiit-operations}

    \begin{mathpar}
        \inferrule
            {x, y : \plotkinPower{D}}
            {x \formalUnion y \Leq{} y \formalUnion x}

        \inferrule
            {x, y, z : \plotkinPower{D}}
            {(x \formalUnion y) \formalUnion z \Leq{} x \formalUnion (y \formalUnion z)}

        \inferrule
            {x : \plotkinPower{D}}
            {x \formalUnion x \Leq{} x}

        \inferrule
            {x : \plotkinPower{D}}
            {x \Leq{} x \formalUnion x}
    \end{mathpar}
    \caption{Inequalities involving operations of $\plotkinPower{D}$}
    \label{fig:power-doamin-qiit-inequalities}
\end{figure}

Next, we work out the instantiation of the QIIT for the signature $\powerSig$.
In essence, the QIIT boils down to the constructors in \cref{fig:power-doamin-qiit-operations,fig:power-doamin-qiit-inequalities}.
However, there are a few remarks to make.
First of all, we do not write down the constructors for the DCPO structure here,
as they are independent of the signature.
Secondly, the way we wrote down the continuity of $\powerIncl{\blank}$ and $\formalUnion$ differs from the original QIIT.
However, under the implicit assumption that both operations are monotone,
this way of writing down continuity is equivalent to how it is written in the original QIIT.
