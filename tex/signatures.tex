\section{Signatures}\label{sec:signatures}

Before we construct DCPO algebras, we need to be able to describe them.
A DCPO algebra is a DCPO, with certain Scott continuous operations,
respecting certain inequalities.
A signature is a collection of these operations and inequalities.
In this section we define this notion of a signature.
Moreover, as a running example, we consider the Plotkin powertheory~\cite{Plotkin76,abramskyJung} for a fixed DCPO $D$.
To complement the definitions, we incrementally build a signature for the Plotkin powertheory of $D$.
The Plotkin powertheory of $D$ has two operations, namely an inclusion from $D$ and a binary operation.%
\footnote{Strictly speaking, the Plotkin powertheory does not have an inclusion operation.
    Normally, the inclusion arises once one takes the free Plotkin powerdomain of $D$.
    However, to better explain the definitions, we also consider an inclusion operation.}
The binary operation has to be commutative, associative and idempotent.

An operation on a DCPO is described using a functor.
We represent this functor using a monomial.
\begin{definition}[Monomial]
    A \textbf{monomial} $\monomial : \Monomial$
    consists of
    \begin{itemize}
        \item an arity $\monomialArity{\monomial} : \Universe$;
        \item a constant DCPO $\monomialConst{\monomial}$.
    \end{itemize}
    We define $\interp{\monomial}$ as the functor which sends a DCPO $X$ to the DCPO
    \begin{gather*}
        (\monomialArity{\monomial} \to X) \times \monomialConst{\monomial}
    \end{gather*}
\end{definition}
Each monomial $\monomial$ describes a type of Scott continuous maps,
as we consider Scott continuous maps of the form
\begin{gather*}
    \interp{\monomial}(X) \to X
\end{gather*}

The Plotkin powertheory of $D$ has two operations:
an inclusion operation $D \to X$,
and a formal union operation $X \to X \to X$.
The types of these two operations can equivalently be written as
\begin{gather*}
    (\bot \to X) \times D \to X \\
    (\Bool \to X) \times 1 \to X
\end{gather*}
The inclusion operation can thus be represented by a monomial with arity $\bot$ and constant DCPO $D$,
and formal union can be represented by a monomial with arity $\Bool$ and the unit DCPO as the constant.

To package multiple operations together, we consider families of monomials.
We call a family of monomials a presignature.
\begin{definition}[Presignature]
    A \textbf{presignature} $\Sigma : \PreSig$ consists of
    \begin{itemize}
        \item a type of constructor names in $\sigConstrNames{\Sigma} : \Universe$;
        \item a family of monomials, $\sigMonomialFam{\Sigma} : \sigConstrNames{\Sigma} \to \Monomial$,
            representing the operations.
    \end{itemize}
    For each presignature, we derive the following two families
    \begin{align*}
        &\sigMonomialArity{\Sigma} : \sigConstrNames{\Sigma} \to \Universe \\
        &\sigMonomialConst{\Sigma} : \sigConstrNames{\Sigma} \to \DCPO
    \end{align*}
    Given a constructor name $a$, the arity and constant DCPO of the monomial $\sigMonomialFam{\Sigma}(a)$,
    are given by $\sigMonomialArity{\Sigma}(a), \sigMonomialConst{\Sigma}(a)$ respectively.
\end{definition}

As the Plotkin powertheory of $D$ has two operations,
we define the type of constructor names of its presignature to be a type with exactly two elements: $\inclusionName$ and $\formalUnionName$.
The family of monomials is constructed by sending both constructor names to their respective monomial we defined above.

Before we introduce how to describe inequalities, let us make the following remark.
\begin{remark}\label{remark:presig-alg}
    Given a presignature $\Sigma$, we are interested in algebras for it.
    There are two equivalent ways to represents these.
    The first way to represent them is by considering a \emph{polynomial} functor $\interp{\Sigma}$,
    sending a DCPO $X$ to
    \begin{gather*}
        \sum_{a : \sigConstrNames{\Sigma}} (\sigMonomialArity{\Sigma}(a) \to X) \times \sigMonomialConst{\Sigma}(a)
    \end{gather*}
    An algebra for this functor consists of a DCPO $X$ together with a map $\interp{\Sigma}(X) \to X$.
    Note that this uses type-indexed coproducts of DCPOs.
    On the other hand, if we curry the polynomial functor $\interp{\Sigma}$,
    we get, for each $a : \sigConstrNames{\Sigma}$, a \emph{monomial} functor sending $X$ to
    \begin{gather*}
        (\sigMonomialArity{\Sigma}(a) \to X) \times \sigMonomialConst{\Sigma}(a)
    \end{gather*}
    Note that this is precisely the functor $\interp{\sigMonomialFam{\Sigma}(a)}$ for each $a : \sigConstrNames{\Sigma}$.
    Thus, we can also represent algebras without mentioning type-indexed coproducts of DCPOs:
    an algebra consists of a DCPO $X$ together with maps $\interp{\sigMonomialFam{\Sigma}(a)}(X) \to X$, for each $a : \sigConstrNames{\Sigma}$.
    In the remainder, we use the second representation of algebras.
\end{remark}

To describe an inequality, we first need a way to describe its left- and right-hand side.
These are both described by the notion of a side:
a term constructed using the operations of a presignature.
\begin{definition}[Side]
    Let $\Sigma$ be a presignature and $V$ a type of variables.
    The type $\Side_{\Sigma, V}$ of \textbf{sides} is inductively generated by the following constructors.
    \begin{mathpar}        
        \inferrule[\textsc{Side-Var}]
            {x : V}
            {\var{x} : \Side_{\Sigma, V}}
            [\textsc{Side-Var}]\label{rule:side-var}

        \inferrule[\textsc{Side-Constr}]
            {a : \sigConstrNames{\Sigma}
                \and f : \sigMonomialArity{\Sigma}(a) \to \Side_{\Sigma, V}
                \and c : \sigMonomialConst{\Sigma}(a)}
            {\constr{a}{f}{c} : \Side_{\Sigma, V}}
            [\textsc{Side-Constr}]\label{rule:side-constr}
    \end{mathpar}
    Each $\var{x}$ represents a variable,
    and $\constr{a}{f}{c}$ represents the application of the operation named $a$ with arguments $f, c$.
    Note that sides are only syntax that describe terms in an algebra.
    We discuss their semantics in \cref{def:sides-interpretation}.
\end{definition}

\begin{remark}
    The type of sides and W-types~\cite{Lof84} seem similar but there are two differences.
    First of all, the type of sides has an additional constructor for variables,
    which allows us to describe terms with variable in them.
    Secondly, the \nameref{rule:side-constr} constructor also takes an additional $c : \sigMonomialConst{\Sigma}(a)$,
    while W-types do not need this. We discuss this difference in more detail in \cref{remark:comparison-w-types-const-dcpo}.
\end{remark}

For the Plotkin powertheory of $D$, we want to be able to describe sides involving the inclusion and formal union operations.
That is, if we take $\Sigma$ to be the presignature we defined earlier, we want the following constructions.
\begin{mathpar}
    \inferrule
        {d : D}
        {\powerIncl{d} : \Side_{\Sigma, V}}

    \inferrule
        {s_1, s_2 : \Side_{\Sigma, V}}
        {s_1 \formalUnion s_2 : \Side_{\Sigma, V}}
\end{mathpar}
Both of these can be constructed using \nameref{rule:side-constr} using specific values for $a,f,c$.
First, let us look at the inclusion side.
As we want to describe the inclusion operation, we take the constructor name $\inclusionName$.
We furthermore take $f$ to be the unique function $\bot \to \Side_{\Sigma, V}$, as $\inclusionName$ has arity $\bot$.
Finally, we need an element of $\sigMonomialConst{\Sigma}(\inclusionName)$, which is $d : D$.

To construct the formal union of two sides, we take the constructor name $\formalUnionName$.
As $\sigMonomialConst{\Sigma}(\formalUnionName) = 1$, we take $c = \tt$.
Finally, as the arity of $\formalUnionName$ is $\Bool$, we need to construct a $f : \Bool \to \Side_{\Sigma, V}$.
We do this by setting $f(\false) = s_1, f(\true) = s_2$.

\begin{remark}
    Note that this specific choice for $f$ is arbitrary,
    as we may as well could have chosen $f(\false) = s_2, f(\true) = s_1$.
    It does not matter which one we choose,
    but we have to make sure that we make the same choice when describing the semantics of these side.
\end{remark}

Now that we can represent both sides of an inequality,
we can describe the inequality itself.
We do this by introducing the notion of a formal inequality.
\begin{definition}[Formal Inequality]
    Let $\Sigma$ be a presignature.
    The type $\Ineq_{\Sigma}$ of \textbf{formal inequalities} has elements which consist of
    \begin{itemize}
        \item a type $V : \Universe$, representing the free variables we can use in the left- and right-hand side;
        \item a left- and right-hand side, both of type $\Side_{\Sigma, V}$.
    \end{itemize}
    We typically write these formal inequalities as $\formalIneqVar{s_1}{V}{s_2}$,
    and often omit the variable type if it is clear from the context.
    Again, note that these formal inequalities do not have any semantical meaning yet.
    Their interpretation is discussed in \cref{def:ineq-validity}.
\end{definition}

In the Plotkin powertheory of $D$, we want the formal union to be commutative, associative and idempotent.
However, these properties are described using equalities, and we can only describe formal inequalities.
The obvious way to describe these equalities would thus be to
introduce two formal inequalities for each equality, as the equality will then follow from the antisymmetry of the ordering relation.
However, it turns out that it is enough to only require the following formal inequalities.
\begin{gather*}
    \formalIneq{x \formalUnion y}{y \formalUnion x} \\
    \formalIneq{(x \formalUnion y) \formalUnion z}{x \formalUnion (y \formalUnion z)} \\
    \formalIneq{x \formalUnion x}{x} \\
    \formalIneq{x}{x \formalUnion x}
\end{gather*}
We now construct these formal inequalities.
As all formal inequalities are constructed in a similar fashion, let us only look at the first one.
It contains two free variables, so we take $V = \Bool$.
We now define the sides representing the variables as $x = \var{\false}, y = \var{\true}$.
We then combine these using $\formalUnion \colon \Side_{\Sigma, V} \to \Side_{\Sigma, V} \to \Side_{\Sigma, V}$,
to create both the left- and right-hand side of the equation.

Finally, we define signatures. They keep track of the operations and formal inequalities we want.
\begin{definition}[Signature]
    A \textbf{signature} $\Sigma : \Sig$ consists of
    \begin{itemize}
        \item a presignature $\sigPre{\Sigma} : \PreSig$;
        \item a type of inequality names $\sigIneqNames{\Sigma} : \Universe$;
        \item a family of formal inequalities $\sigIneqFam{\Sigma} : \sigIneqNames{\Sigma} \to \Ineq_{\sigPre{\Sigma}}$.
    \end{itemize}
    We often think of $\sigPre{\Sigma}$ being a substructure of $\Sigma$.
    This allows us, for example, to speak about the type of constructor names $\sigConstrNames{\Sigma}$, even if $\Sigma$ is a signature.
    All in all, a signature consists of
    \begin{itemize}
        \item a type of constructor names, $\sigConstrNames{\Sigma}$;
        \item a family of monomials, $\sigMonomialFam{\Sigma}$;
        \item a family of the arities of the operations, $\sigMonomialArity{\Sigma}$;
        \item a family of the constant DCPOs of the operations, $\sigMonomialConst{\Sigma}$;
        \item a type of inequality names, $\sigIneqNames{\Sigma}$;
        \item a family of formal inequalities, $\sigIneqFam{\Sigma}$.
    \end{itemize}    
\end{definition}

At last, we construct a signature for the Plotkin powertheory of $D$.
We already defined its presignature.
We define $\sigIneqNames{\Sigma}$ as a four element type, as we want four formal inequalities.
The family of formal inequalities is defined using the formal inequalities we described above.