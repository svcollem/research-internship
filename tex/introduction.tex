\section{Introduction}

The field of domain theory was started in the late 1960s by Dana Scott~\cite{Scott70}.
At the time it was used to describe the denotational semantics of the lambda calculus.
In particular, because lambda abstractions can be given as arguments to other lambda abstractions,
the domain theory needed to
(1) describe a set interpreting the syntax,
(2) a set of functions between these interpretations,
(3) and a correspondence between these two sets.
Furthermore, to model all the fixed-point combinations in the lambda calculus,
the fixpoints of all functions in (2) need to exist.

Since these times, programming languages have evolved in a rapid pace, introducing many new paradigms.
Algebraic effects~\cite{lmcs:705} have been introduced to represent computational effects such as
state, exceptions, nondeterminism, non-termination, input-output, and many more.
They allow to factor out effectful computations from pure computations
and can be composed easily.
An effect is defined as a set of operations and an (in)equational theory these operations should obey.
For example, non-termination has two operations; one operations represents the non-termination,
while the other represents the possible returned value.
The inequational theory states that the non-termination operation should be smaller than all possible return values,
as non-termination gives the least amount of information about the return value of a partial function.

To keep up with the rapid development of these new constructs in programming languages,
and to fully understand how they work,
we also need to be able to describe these algebraic effects using denotational semantics.
In particular, we need to be able to construct these algebraic effects using domain theory.
They are constructed as certain DCPO algebras:
a DCPO $D$ together with operations on $D$ which obey an inequational theory.

In this report, we follow an approach taken by \citet{Partiality} to construct algebraic effects in domain theory.
\citet{Partiality} used a quotient inductive-inductive type (QIIT)~\cite{DBLP:journals/corr/AltenkirchCDF16},
a construction from homotopy type theory,
to construct the non-termination effect as a DCPO algebra in domain theory.
In this report, we extend their method and present a general framework to construct algebraic effects in domain theory.
We first describe a method to represent various algebraic effects, using signatures.
We then show how we can construct algebraic effects as the initial DCPO algebra for its signature,
following a similar approach to \citet{Partiality} using QIITs.
We show that several well-known examples of algebraic effects fit our framework.

As a foundation, our work uses homotopy type theory (HoTT).
In particular, we use QIITs and some of the arguments make use of function extensionality.
We do not need to use the univalence axiom in our work.
Our work is formalized using Cubical Agda~\cite{cubical}
and can be found at \url{https://gitlab.science.ru.nl/svcollem/research-internship}.

We start this report by discussing various ways to construct complete partial orders (CPOs) in \cref{sec:constructing-cpos}.
In \cref{sec:signatures} we present how we can represent algebraic effects using signatures.
The DCPO algebras corresponding to a signature are discussed in \cref{sec:algebras}.
Furthermore, we present examples of DCPO algebras at the end of \cref{sec:algebras}.
We then show how we can use QIITs to construct the algebraic effects as the initial DCPO algebra for its signature in \cref{sec:initial-algebra}.
Finally, we end by discussing various examples of algebraic effects at the end of \cref{sec:initial-algebra}.
