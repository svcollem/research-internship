#!/bin/bash

set -xe

agda --latex --latex-dir=build slides.lagda
cd build
lualatex -shell-escape slides.tex
cd -
