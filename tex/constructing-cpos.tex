\section{Constructing CPOs}\label{sec:constructing-cpos}

Before we discuss various ways to construct CPOs,
let us compare two different definitions of CPOs.
The first definition we consider is a $\omega$-chain complete partial order ($\omega$-CCPO).
This is a partial order where every increasing sequence $(d_i)_{i \in \mathbb{N}}$,
has a least upper bound. On the other hand, we also have a notion of a directed complete partial order (DCPO).
A family $\alpha : I \to D$ is directed if $I$ is inhabited,
and for any indices $i, j : I$, there exists an index $k : I$ such that $\alpha(i), \alpha(j) \sqsubseteq \alpha(k)$.
A DCPO is a partial order such that any directed family has a least upper bound.
It is easy to show that every DCPO is also an $\omega$-CCPO.
The other direction is more involved and requires the axiom of choice~\cite{Markowsky1976ChaincompletePA}.
In this report, we work with DCPOs.
One reason is that when working with $\omega$-CCPOs,
one often needs to use the axiom of choice to construct a sequence indexed by a set.

We now discuss several constructions for DCPOs.
Note that one could also use similar constructions to construct $\omega$-CPPOs.

\paragraph{Rounded Ideal Completion}
One way to construct DCPOs is via the \emph{rounded ideal completion}.
Elements of a DCPO typically approximate some form of information,
especially in the context of semantics of programming languages.
Using the rounded ideal completion, one starts off with an \emph{abstract basis}
and completes it to get a DCPO.
\begin{definition}[Abstract basis]
    A pair $(B, \prec)$ is an \textbf{abstract basis} if $\prec$ is transitive,
    and it has the following interpolation properties:
    \begin{itemize}
        \item if $x : B$, then there exists $y : B$ such that $y \prec x$;
        \item if $x_1, x_2, z : B$ such that $x_1, x_2 \prec z$, then there exists $y : B$ such that $x_1, x_2 \prec y \prec z$.
    \end{itemize}
\end{definition}
We now consider rounded ideals of such an abstract basis $(B, \prec)$.
These ideals form the underlying type of the rounded ideal completion of $B$.
Following the intuition that these ideals should approximate information,
they are defined as directed lower-subsets of $B$.
\begin{definition}[Rounded ideal]
    Let $(B, \prec)$ be an abstract basis. A subset $X \subseteq B$%
    \footnote{Note that we think of $X$ as a predicate $X \colon B \to \Omega$ on $B$.}
    is a \textbf{rounded ideal} if the following hold:
    \begin{itemize}
        \item $X$ is inhabited;
        \item $X$ is a lower set w.r.t. $\prec$, i.e. if $y \in X$ and $x \prec y$ then $x \in X$;
        \item $X$ contains upperbounds, i.e. if $x_1, x_2 \in X$, then there exists some $y \in X$ such that $x_1, x_2 \prec y$
    \end{itemize}
    The type of rounded ideals, $\RoundedIdeal{B, \prec}$,
    forms a DCPO with subset inclusion as the ordering and union of subsets as the supremum.
\end{definition}
As an example, one can start of with rational intervals ordered by reverse strict inclusion as an abstract basis,
and use the rounded ideal completion to get intervals of reals numbers~\cite{vanderWeide2024}.

\paragraph{Presentation}
Next up, we consider presentations of DCPOs~\cite{JUNG2008209}.
The rounded ideal completion lacks the ability to force extra inequalities to hold.
In a case where this is desired, we can use DCPO presentations.

A DCPO presentation consists of a preorder $P$,
and a cover relation $\covered$ on the product of $P$ and directed subsets of $P$.
Intuitively, the preorder $P$ contains the generator elements for the DCPO we want to present.
Whenever a generator $p$ is covered by a directed subsets $U$, written $p \covered U$,
we intuitively think of $p$ being below the supremum of $U$ in the presented DCPO.

Now we say that $I \subseteq P$ is an \emph{ideal} if it is a lower-set,
and for each $p \covered U$, $U \subseteq I$, we have $p \in I$.
Th type of ideals, $\Ideal{P}$, has a complete lattice structure.
For a subset $M \subseteq P$, we write $\langle M \rangle$ for the least ideal containing $M$.

We can then define the presented DCPO $\overline{P}$ as the least sub-DCPO of $\Ideal{P}$,
containing all $\langle \{ q \mid q \leq p \} \rangle$ for $p : P$.

This technique allows us to quotient DCPOs by inequalities.
Given a DCPO $D$, we can force inequalities by adding coverings of the form $x \covered \{ y \}$.
This gives us, for example, the coequalizer of DCPOs.

\paragraph{Quotient Inductive-Inductive Type}
Lastly, we can use quotient inductive-inductive types (QIITs)~\cite{DBLP:conf/fossacs/AltenkirchCDKF18} to construct DCPOs.
A QIIT is a generalization of a quotient inductive type and a inductive-inductive type.
In the context of DCPOs, we define by mutual recursion a type $D$ and a predicate ${\sqsubseteq} : D \to D \to \Universe$,
by writing down their constructors.
The types $D$ and $\sqsubseteq$ represent the carrier and ordering of the DCPO respectively.

Just like with DCPO presentations, we can force inequalities to hold in $D$,
by adding additional constructors to the ordering relation $\sqsubseteq$.

In this report, we work with QIITs to construct initial DCPO algebras.
We discuss them in more detail in \cref{sec:initial-algebra}.
